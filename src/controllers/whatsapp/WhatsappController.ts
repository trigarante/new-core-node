import {Request, Response} from "express";
import {getManager, getRepository} from "typeorm";
import {moResponse} from '../../entity/whatsapp/MoResponse';
import {empleadoView} from "../../entity/whatsapp/EmpleadoView";
import {canalWhatsapp} from "../../entity/whatsapp/canalWhatsapp";
import {SolicitudesViewSinJson} from "../../entity/whatsapp/SolicitudesViewSinJson";
import {mensajesWhatsapp} from "../../entity/whatsapp/mensajesWhatsapp";
import {TipoMensajeWhatsapp} from "../../entity/whatsapp/tipoMensajeWhatsapp";
// import * as IO from '../../index';
// import {tipoRoom} from '../../index';
import {CatalogoNumerosWavy} from "../../entity/whatsapp/catalogoNumerosWavy";
import {PlantillaComercialWhatsapp} from "../../entity/whatsapp/PlantillasComercialWhatsapp";
import {CotizacionesAli} from "../../entity/whatsapp/CotizacionesAli";
import {Socios} from "../../entity/whatsapp/socios";
import {MarcasEmpresa} from "../../entity/whatsapp/marcasEmpresa";
import {Socket} from 'socket.io';
import {TipoSubareaPlantillaWhatsappView} from "../../entity/whatsapp/TipoSubareaPlantillaWhatsappView";
import {urlSockets} from "../../urlSocket";
import {EmpleadosMensajesWhatsappView} from "../../entity/whatsapp/EmpleadosMensajesWhatsappView";
import {SupervisoresSubareaWhatsappView} from "../../entity/whatsapp/SupervisoresSubareaWhatsapp";
import {Subarea} from "../../entity/whatsapp/Subarea";
import {TipoSubarea} from "../../entity/whatsapp/tipoSubarea";
import {SubareasPlantillaActivaView} from "../../entity/whatsapp/SubareasPlantillaActivaView";
import {SubareasOperacionWhatsappView} from "../../entity/whatsapp/SubareasOperacionWhatsappView";
import {EjecutivosOperacionWhatsappView} from "../../entity/whatsapp/EjecutivosOperacionWhatsappView";
// import * as fetch from 'node-fetch';
const https = require('https');
const http = require('http');
const fetch = require('node-fetch');
const request = require('request');
let dataIO: Socket;

class WhatsappController{
    /** Se guarda la respuesta de wavi en la tabla moResponse.
     * Después se manda llamar la funcion postCanalWhats **/
    static newMessage = async (req: Request, res: Response) => {
        let { total, data } = req.body;

        const idWavy = req.body.data[0].id;
        const idSolicitud = req.body.data[0].correlationId;

        let moresponse = new moResponse();
        moresponse.total = total;
        moresponse.data = data;
        moresponse.idWavy = idWavy;
        moresponse.idSolicitud = req.body.data[0].correlationId;

        const solicitudesViewRepository = getRepository(SolicitudesViewSinJson);
        const solicitudes = await solicitudesViewRepository.find({
            where: {id: idSolicitud}
        });

        const idEmpleado = solicitudes[0].idEmpleado;
        req.params = {idEmpleado: idEmpleado};

        try {
            await getRepository(moResponse).save(moresponse);
            req.body = {...req.body, 'idTipoEnvio': 2}; // para saber que un mensaje viene del usuario
            const dStatus = await Object.keys(req.body).some(function(deliveredStatus) {
                return deliveredStatus === 'deliveredStatus';
            });
            if (dStatus) {
                // await WhatsappController.postCanalWhatsPlantilla(req, res);
            } else {
                await WhatsappController.postCanalWhats(req, res);
            }
            res.status(200);
        } catch (e) {
            console.log(e, 'errooooor 1 /save/mo-response');
            res.status(400).send({message: "Bad request", error: e});
            return;
        }
    };
    /**
     * Manda unicamente mensajes **/
    static whatsappSend = async (req: Request, res: Response) => {
        const headers = {
            'UserName': `${req.body.credentials.userName}`,
            'AuthenticationToken': `${req.body.credentials.authToken}`,
            // 'UserName': 'wa_ahorraseguros2_mx',
            // 'AuthenticationToken': 'RKrlOV0q3nYpu1RnbwQTU5qihFoNJuIELumP4Okh',
        };
        // otra manera de hacer la petición sin libreria
        const data = JSON.stringify(req.body.message);
        // const data = JSON.stringify(req.body);
        const options: any = {
            headers: headers,
            method: 'POST',
            path: '/v1/whatsapp/send',
            body: JSON.stringify(req.body),
            host: 'api-messaging.wavy.global',
        };
        const reeq = https.request(options, rees => {
            let payload = '';

            // se reciben los pedazos de info y se añade a 'str'
            rees.on('data', d => {
                payload += d;
            });
            // toda la respuesta ha sigo recibida, se manda en formato json
            rees.on('end', () => {
                res.json(JSON.parse(payload));
            });
        });
        reeq.on('error', error => {
            // manejador de errores
            const erroor = {...error, mensaje: 'algo salió mal'};
            res.send(erroor);
        });
        reeq.write(data);
        reeq.end();
    };
    /**
     * Manda unicamente polantillas **/
    static whatsappSendPlantillas = async (req: Request, res: Response) => {
        /*
        * @params idDiferenciaNumerosTest para identificar los 5 numeros de las diferentes campañas activas
        *   las cuales son Ahorra Seguros y Qualitas Brand.
        *   En la tabla de CatalogoNumerosWavy ya se añadieron los 5 nuevos números.
        * */
        // console.log(req.body);
        // console.log(req.body, 'req.body whatsappSend');
        const idDiferenciaNumerosTest = req.body.idDiferenciaNumerosTest;
        const idSocio = req.body.idSocio;
        const idEmpresaMarca = req.body.idMarcaEmpresa;
        const idEmpleado = req.body.idEmpleado;
        const idSolicitud = req.body.messages.destinations[0].correlationId;
        let numeroDestino = req.body.messages.destinations[0].destination;
        numeroDestino = numeroDestino.slice(3);

        let imagen;

        if (req.body.idSocio) {
            imagen = await getRepository(Socios).findOne({id: +idSocio, activo: 1}).then(socio => socio.imagenPlantilla);
        } else {
            imagen = await getRepository(MarcasEmpresa).findOne({id: +idEmpresaMarca, activo: 1}).then(marca => marca.imagenPlantilla);
        }

        const header = {
            'header': {
                'image': {
                    'url': imagen,
                    'type': 'PNG',
                },
            },
        };

        const {messages} = req.body;
        const {message} = messages;
        const {template} = message;

        // aqui es para concatenar los datos de la plantilla con el idEmpleado en la funcion getPlantillaByEmpleadoIdTipoSubarea()

        const dataPlantillasPorTipoSubarea = await getPlantillaByEmpleadoIdTipoSubarea(idEmpleado, res);
        // console.log(dataPlantillasPorTipoSubarea[0], 'dataPlantillasPorTipoSubarea');
        template.elementName = dataPlantillasPorTipoSubarea[0].nombrePlantilla;

        const dataParametros = await getNombreEmpleadoNombreProspecto(idEmpleado, idSolicitud).catch(e => {
            res.status(200).send({
                error: `No se encontró o no existe la solicitud ${idSolicitud}, con empleado ${idEmpleado}, numero de destino ${numeroDestino}`,
                message: e,
            });
            return;
        });

        const {
            nombre, apellidoPaterno, apellidoMaterno, nombreProspecto
        } = dataParametros;

        template.bodyParameters[0] = `${nombreProspecto}`;
        template.bodyParameters[1] = `${nombre} ${apellidoPaterno} ${apellidoMaterno}`; // Nombre del empleadoo

        // template.language = 'es_MX';
        template.languagePolicy = 'DETERMINISTIC';
        template.namespace = '0afd3994_d014_45ab_830e_1613b060a7ec';

        // ***********************************************************************************************************************

        // const templateConHeader = {...template, ...header};
        const templateConHeader = {...template};
        req.body.messages.message.template = {...templateConHeader};
        // console.log(JSON.stringify(req.body), 'req.bodyreq.bodyreq.body')
        // a partir de aquí se tiene que generar la propiedad credentials del json


        // 1.- Se busca si el canal ya existe
        req.params.numero = numeroDestino;
        const dataCanal: any = await buscarCanal(req, res).then(res => res);
        const dataCatalogoNumerosWavy: any = await getCatalogoNumerosWavy(idEmpresaMarca).then(data => data);
        // console.log(dataCatalogoNumerosWavy, 'dataCatalogoNumerosWavydataCatalogoNumerosWavy');
        let credentials;
        if (dataCanal.found === 0) {
            credentials = numeroDataRandom(dataCatalogoNumerosWavy, null, numeroDestino);
            const jsonCredential = {
                credentials: {...credentials}
            };
            // console.log(JSON.stringify(credentials), 'credentialscredentialscredentials')
            req.body = {...req.body, ...jsonCredential};
        } else {
            let numerosRestantes;
            const last = dataCanal.data.length - 1;
            const numeroOrigen = dataCanal.data[last].origen;
            const numeroDestino = dataCanal.data[last].destino;

            const dataOrigenDestino: any = await getOrigenDestino(numeroDestino, numeroOrigen);
            numerosRestantes = dataOrigenDestino.numerosRestantes;

            if (numerosRestantes.length !== 0) {
                const numerosRestantesData = numeroDataRandom(numerosRestantes, null, numeroDestino);

                req.body = {...req.body, credentials: {numerosRestantes: {}, numeroGenerador: numerosRestantesData.numeroGenerador, userName: numerosRestantesData.userName, namespace: numerosRestantesData.namespace, authToken: numerosRestantesData.authToken}};
                req.body.credentials.numerosRestantes = numerosRestantes;
            } else {
                res.status(200).send({
                    status: `El número ${numeroDestino} ya alcanzó el límite permitido al usar ya los 5 números disponibles`,
                    message: 'Sin numeros disponibles',
                    sinNumerosTelefonicos: true
                });
                return;
            }
        }

        const headers = {
            'UserName': `${req.body.credentials.userName}`,
            'AuthenticationToken': `${req.body.credentials.authToken}`,
        };
        const data = JSON.stringify(req.body.messages);
        // console.log(JSON.stringify(req.body), 'datadatadata');
        const options: any = {
            headers: headers,
            method: 'POST',
            path: '/v1/whatsapp/send',
            body: JSON.stringify(req.body),
            host: 'api-messaging.wavy.global',
        };
        const reeq = https.request(options, rees => {
            let payload = '';

            // se reciben los pedazos de info y se añade a 'str'
            rees.on('data', d => {
                payload += d;
            });
            // toda la respuesta ha sigo recibida, se manda en formato json
            rees.on('end', async () => {
                await WhatsappController.postCanalWhatsPlantillaExterno(req, res).then((pcwpe: any) => {
                    res.status(200).send({canalWhatsapp: pcwpe.canalWhatsapp, mensajesWhatsapp: pcwpe.mensajesWhatsapp});
                });
            });
        });
        reeq.on('error', error => {
            // manejador de errores
            const erroor = {...error, mensaje: 'Plantilla no enviada'};
            res.send(erroor);
        });
        reeq.write(data);
        reeq.end();
    };
    /***************Guardar datos que vienen desde externo*********************************/
    static postCanalWhatsPlantillaExterno = async (req: Request, res: Response) => {
        // número del origen (teléfono celular con clave de país "521")
        let numeroDestino = req.body.messages.destinations[0].destination;
        const solicitud = req.body.messages.destinations[0].correlationId;
        const nombrePlantillaEnviada = req.body.messages.message.template.elementName;
        const idEmpresaMarca = req.body.idMarcaEmpresa;
        const idEmpleado = req.body.idEmpleado;

        const { numeroGenerador, numerosRestantes } = req.body.credentials;

        // quita el 521 de los números para poder hacer la búsqueda en la tabla de solicitudes
        numeroDestino = numeroDestino.slice(3);

        let canalGuardado: any = {};

        try {

            let cWhatsapp = new canalWhatsapp();
            cWhatsapp.idEstadoCanal = 1;
            cWhatsapp.idSolicitud = solicitud;
            cWhatsapp.respuestaMensajesMo = `Se envió la plantilla ${nombrePlantillaEnviada}`;
            cWhatsapp.cantidadMo = 1;
            cWhatsapp.destino = numeroDestino;
            cWhatsapp.fechaEnvio = new Date();
            cWhatsapp.origen = numeroGenerador;
            cWhatsapp.numerosRestantes = numerosRestantes;
            cWhatsapp.idSocio = req.body.idSocio;
            cWhatsapp.idMarcasEmpresa = req.body.idEmpresaMarca;
            cWhatsapp.activacion = 1;
            cWhatsapp.idMarcasEmpresa = idEmpresaMarca;

            canalGuardado = await getRepository(canalWhatsapp).save(cWhatsapp).catch(e => {
                manejoErrores('error', res, 500, e);
                return;
            });

            let mensajesWha = new mensajesWhatsapp();
            mensajesWha.idCanalWhatsapp = canalGuardado.id;
            mensajesWha.idEmpleadoEnvio = idEmpleado;
            mensajesWha.idTipoEnvio = 1; // SALIENTE
            mensajesWha.idTipoMensajeWhatsapp = 7; // PLANTILLA
            mensajesWha.mensaje = `Se envió la plantilla ${nombrePlantillaEnviada}`;
            mensajesWha.fecha = new Date();
            mensajesWha.correlationId = solicitud;
            mensajesWha.aBandeja = 0;
            mensajesWha.idEmpleadoMensajes = idEmpleado;

            const mensajesWhats = await getRepository(mensajesWhatsapp).save(mensajesWha).catch(e => {
                manejoErrores('error', res, 500, e);
                return;
            });

            // manda mensaje privado
            let json = {};

            json = {...mensajesWha, idTipoEnvio: mensajesWha.idTipoEnvio, mensaje: 'Plantilla enviada'};
            /* identifica a qué socket tiene que emitir los mensajes que llegan (IO.io.to). room_${req.body.data[0].correlationId} es una
               sala que se maneja con el idSolicitud (req.body.data[0].correlationId) para mandar mensajes privados*/
            // IO.io.to(`room_${solicitud}`).emit(`mensaje-nuevo${solicitud}`, json);

            json = {
                ...mensajesWha,
                idTipoEnvio: mensajesWha.idTipoEnvio,
                mensaje: `Se envió la plantilla ${nombrePlantillaEnviada}`,
                tipoMensaje: 2,
                notificacion: 1,
                cantidadMensajesIndividuales: [{solicitud: 1}],
                socketNumero: 4,
                solicitudDestino: solicitud,
            }; // AQUIIIIIIIIIII
            // IO.io.to(`room_solicitudes_${idEmpleado}`).emit(`msg-solicitudes-${idEmpleado}`, json);
            // sendSocket(req, res);

            // se manda al front la información de las tablas canalWhatsapp y mensajesWhatsaoo
            // res.status(200).send({canalWhatsapp: canalGuardado, mensajesWhatsapp: mensajesWhats});
            return {canalWhatsapp: canalGuardado, mensajesWhatsapp: mensajesWhats};
        } catch (e) {
            res.send({message: "bad request", error: e});
            return false;
        }
    };
    /**
     * envío de sms **/
    static sendSMS = async (req: Request, res: Response) => {
        const headers = {
            'UserName': 'wa_ahorraseguros_mx',
            'AuthenticationToken': 'RL02Z-AfUx-kr8c57LkvSDHTCZq2nQoY1jQSKOxo',
        };
        const destination = req.query.destination;
        const messageText = req.query.messageText;
        const params = {
            "destination": destination,
            "messageText": messageText
        };

        const query = Object.keys(params)
        // encodeURIComponent reemplaza los caracteres para que sea una cadena continua.
        /*Ejemplo
        * const ejem1 = ";,/?:@&=+$";
        * console.log(encodeURIComponent(ejem1)); // %3B%2C%2F%3F%3A%40%26%3D%2B%24*/
            .map(llaves => encodeURIComponent(llaves) + '=' + encodeURIComponent(params[llaves]))
            /* Al final une cada propiedad del json "params" con un "&".
            * destination=521555555555&messageText=HOLA%2C%20ES%20UNA%20PRUEBA */
            .join('&');

        const url = `https://api-messaging.wavy.global/v1/send-sms?${query}`; // se agrega "?" para que reconozca el queryParams
        fetch(url, {method: 'GET', headers: headers})
            .then(resp => resp.json())
            .then( async json => {
                const newJson = {...json, ...params};
                // const jsonMensajesWhatsapp = {...newJson, data: [{origin: destination, message: {messageText: messageText}}], idTipoEnvio: 2, idEmpleadoEnvio: '' }; // para que los mensajes se almacenen en la tabla y manejen la misma estructura
                // const jsonMensajesWhatsapp = {...newJson, data: [{origin: destination, message: {messageText: messageText}}], idTipoMensajeWhatsapp: 2, idEmpleadoEnvio: null }; // para que los mensajes se almacenen en la tabla y manejen la misma estructura. el idTipoMensajeWhatsapp 2 es SMS
                // await WhatsappController.postCanalWhats(jsonMensajesWhatsapp, res); // para que los mensajes se almacenen en la tabla y manejen la misma estructura
                res.json(newJson)
            }).catch( e => {
        });
    };

    static pruebaRest = async (req: Request, res: Response) => {
        /*const headers = {
            'UserName': 'wa_ahorraseguros_mx',
            'AuthenticationToken': 'RL02Z-AfUx-kr8c57LkvSDHTCZq2nQoY1jQSKOxo',
        };
        const url = 'https://api-messaging.wavy.global/v1/whatsapp/send';
        const cuerpo = JSON.stringify(req.body);
        fetch(url, {method: 'POST', headers: headers, body: cuerpo})
            .then(res => res.json())
            .then( json => {
                res.json(json)
            }).catch( e => {
        });*/
        // console.log(req.body);
        // const id = req.params.id;
        //
        // // IO.io.emit('mensaje-nuevo', 'hola desde rest');
        const msg = {
            idCanalWhatsapp:"1",
            idEmpleadoEnvio:null,
            idTipoEnvio:2,
            idTipoMensajeWhatsapp:"1",
            mensaje:"111!!",
            fecha:"2021-04-26T17:24:37.842Z",
            correlationId:"165278",
            aBandeja:1,
            idEmpleadoMensajes:"6054",
            id:19,
            tipoMensaje:"TEXT",
            notificacion:1,
            cantidadMensajesIndividuales:{"165278":19},
            socketNumero:4,
            solicitudDestino:"165278"};
        // IO.io.emit('mensaje-nuevo165278', msg); // mensaje a todo el que se conecte
        //
        // // manda mensaje privado
        // IO.io.to(`room_${req.body.data[0].correlationId}`).emit('mensaje-nuevo', msg.mensaje);
        res.json({status: 'ok'});
    };
    /**
     * Obtiene las plantilla de acuerdo al tipo de subarea**/
    static getPlantillaByIdEmpleadoTipoSubarea = async (req: Request, res: Response) => {
        const idEmpleado = req.params.idEmpleado;

        const tipoPlantillaRepository = getRepository(PlantillaComercialWhatsapp);
        const empleadoRepository = getRepository(empleadoView);

        try {
            const empleado: any = await empleadoRepository.findOneOrFail({
                where: {id: Number(idEmpleado)}
            }).catch(e => {
                manejoErrores('No se encontró el idEmpleado', e, 404);
            });
            const tipoPlantilla = await tipoPlantillaRepository.find({
                where: {
                    idTipoSubarea: empleado.idTipoSubarea,
                    activa: 1,
                }
            }).catch(e => {
                manejoErrores('No se encontró tipoPlantilla', e, 404);
            });
            res.send(tipoPlantilla);
        } catch (e) {
            res.status(500).send("request not found");
        }
        /*const tipoPlantillaRepository = getRepository(plantillasWhatsApp);
        const empleadoRepository = getRepository(empleadoView);


        try {
            const empleado: any = await empleadoRepository.findOneOrFail({
                where: {id: Number(idEmpleado)}
            }).catch(e => {
                manejoErrores('No se encontró el idEmpleado', e, 404);
            });
            const tipoPlantilla = await tipoPlantillaRepository.find({
                where: {idTipoSubarea: empleado.idTipoSubarea}
            }).catch(e => {
                manejoErrores('No se encontró tipoPlantilla', e, 404);
            });
            res.send(tipoPlantilla);
        } catch (e) {
            res.status(500).send("request not found");
        }*/
    };
    /** Obtiene el historico de los mensajes por canal **/
    static getMensajesByIdCanal = async (req: Request, res: Response) => {
        const idSolicitud = req.query.idSolicitud;
        const canalWhatsappRepository = getRepository(canalWhatsapp);
        const mensajesWhatsappRepository = getRepository(mensajesWhatsapp);
        const catalogoNumerosWavy = getRepository(CatalogoNumerosWavy);
        const solicitudesRepository = getRepository(SolicitudesViewSinJson);

        try {
            const canal: any = await canalWhatsappRepository.findOne({
                where: {idSolicitud: idSolicitud}
            });
            const solicitudeData = await solicitudesRepository.findOne({
                where: {id: idSolicitud}
            });
            const canalConNumeroWavy: any = await canalWhatsappRepository.find({
                where: {destino: solicitudeData.numeroProspecto}
            });
            const cantidadPlantillas = canalConNumeroWavy.length;
            if (!canal) {
                return res.json({
                    cuerpo: null,
                    mensaje: `No se encontró ningún canal con el idSolicitud: ${idSolicitud}`,
                    cantidadDeEnvioDePlantillas: cantidadPlantillas
                });
            }
            const mensajes = await mensajesWhatsappRepository.find({
                where: {idCanalWhatsapp: canal.id}
            });
            if (!mensajes) {
                return res.json({
                    cuerpo: null,
                    mensaje: 'Todavia no hay mensajes en el canal',
                });
            }
            if (canal && mensajes) {
                const dataCatalogo = await catalogoNumerosWavy.findOne({
                    where: { numeroTelefono: canal.origen}
                });
                res.send({mensajes: mensajes, catalogoData: dataCatalogo, canalData: canal, solicitudData: solicitudeData});
            }
        } catch (e) {
            manejoErrores(e, res, 500);
        }
    };
    static getMensajesAndCanal = async (req: Request, res: Response) => {
        const idEmpleado = req.params.idEmpleado;
        const mensajesWhatsappRepository = getRepository(mensajesWhatsapp);

        try {
            const mensajes = await mensajesWhatsappRepository.find({
                where: {aBandeja: 1, idTipoEnvio: 2, idEmpleadoMensajes: idEmpleado}
            });
            const mensajesFiltroHora = mensajes.filter( data => {
                const date = new Date(data.fecha);
                const dateConHoras = new Date(data.fecha).setHours(24);
                const dateActual = new Date();
                const dateMsecActual = dateActual.getTime();

                const fechasData = timeConversion1(date, dateConHoras, dateMsecActual);
                return fechasData.milisecActual < fechasData.milisecFechaRegistroMasHoras;
            });
            const noRespondidosArr = [];
            mensajesFiltroHora.forEach(dataMsg => {
                noRespondidosArr.push(dataMsg.correlationId);
            });
            /* cuenta cuantos mensajes tienen por responder cada canal
             { '161552': 1, '161553': 1, '161555': 2 } */
            const noRespondidosArrSorted = noRespondidosArr.sort();
            let counter = {};
            for(let i = 0; i < noRespondidosArrSorted.length; i++) {
                if(!(noRespondidosArrSorted[i] in counter))counter[noRespondidosArrSorted[i]] = 0;
                counter[noRespondidosArrSorted[i]]++;
            }
            // console.log(counter, 'counter')
            // **********************************************************************************************************
            const setNoRespondidosArr = new Set(noRespondidosArr);

            const dataNoRepetida = noRespondidosArr.filter( onlyUnique1 );

            // pendiente, para poder sacar la fecha y mostrarla en la bandeja
            const paraFechaArr = [];
            const dataClasificada = dataNoRepetida.map(d => {
                return mensajesFiltroHora.filter(val => {
                    return val.correlationId === d
                });
            });
            for (let i = 0; i < dataClasificada.length; i++) {
                for (let j = 0; j < dataClasificada[i].length; j++) {
                    if (dataClasificada[i].length > 1 ) {
                        paraFechaArr.push(dataClasificada[dataClasificada.length - 1]);
                    } else {
                        paraFechaArr.push(dataClasificada[0]);
                    }
                }
            }
            // console.log(paraFechaArr, 'paraFechaArr');
            // ********************************************************************
            if (mensajes) {
                const json = {
                    mensajes: dataNoRepetida,
                    cantidadNotificaciones: setNoRespondidosArr.size,
                    socketNumero: 3,
                }; // AQUIIIIIIIIIII
                req.body = {...req.body, ...json};
                // sendSocket(req, res);
                // if (tipoRoom === 'pendientes') {
                    /*IO.io.emit(`msg-pendientes-${idEmpleado}`, {
                        mensajes: dataNoRepetida,
                        cantidadNotificaciones: setNoRespondidosArr.size,
                        cantidadMensajesIndividuales: counter,
                    });*/
                    // sendSocket({
                    //     mensajes: dataNoRepetida,
                    //     cantidadNotificaciones: setNoRespondidosArr.size,
                    //     cantidadMensajesIndividuales: counter,
                    //     idEmpleado: idEmpleado,
                    //     socket: 'mensajes-and-canal',
                    // }, res);
                // }
                /*if (tipoRoom === 'pendientes') {
                    IO.io.emit(`msg-pendientes-${idEmpleado}`, {
                        mensajes: dataNoRepetida,
                        cantidadNotificaciones: setNoRespondidosArr.size,
                        cantidadMensajesIndividuales: counter,
                        dataClasificada: dataClasificada,
                    });
                }*/
                res.send({mensajes: dataNoRepetida, cantidadNotificaciones: setNoRespondidosArr.size, cantidadMensajesIndividuales: counter, dataClasificada: dataClasificada});
            }
        } catch (e) {
            manejoErrores(e, res, 500);
        }

        function onlyUnique1(value, index, self) {
            return self.indexOf(value) === index;
        }

        function timeConversion1(fecha, fechaMasHora, dateMsec) {
            const msecPerMinute = 1000 * 60;
            const msecPerHour = msecPerMinute * 60;
            const msecPerDay = msecPerHour * 24;

            const milisecFechaRegistro = fecha / msecPerDay;
            const milisecFechaRegistroMasHoras = fechaMasHora / msecPerDay;
            const milisecActual = dateMsec / msecPerDay;

            return {
                milisecFechaRegistro: milisecFechaRegistro,
                milisecFechaRegistroMasHoras: milisecFechaRegistroMasHoras,
                milisecActual: milisecActual
            }
        }
    };
    static mensajeRespondidoSolicitudes = async (req: Request, res: Response) => {
        const idEmpleado = req.params.idEmpleado;
        const mensajesData = req.body;
        const mensajesWhatsappRepository = getRepository(mensajesWhatsapp);
        const mensajesActualizados: any[] = [];

        for (let i = 0; i < mensajesData.length; i++) {
            try {
                mensajesData[i] = {...mensajesData[i], aBandeja: 0};
                const mensajes = await mensajesWhatsappRepository.update(mensajesData[i].id, mensajesData[i]);
                mensajesActualizados.push(mensajes);
            } catch (e) {
                manejoErrores(e, res, 500);
            }
        }
        await getMensajesAndCanal().then( val => {
            res.status(200).json({mensajesActualizados: mensajesActualizados});
        }).catch(e => {
            console.log(e, 'eeeeeeeeeerrrrrrrrrrrrrrrroooooooooooor 1111111111111111');
        });

        async function getMensajesAndCanal() {
            const mensajesWhatsappRepository = getRepository(mensajesWhatsapp);

            try {
                const mensajes = await mensajesWhatsappRepository.find({
                    where: {aBandeja: 1, idTipoEnvio: 2}
                });
                const mensajesFiltroHora = mensajes.filter(data => {
                    const date = new Date(data.fecha);
                    const dateConHoras = new Date(data.fecha).setHours(24);
                    const dateActual = new Date();
                    const dateMsecActual = dateActual.getTime();

                    const fechasData = timeConversion(date, dateConHoras, dateMsecActual);
                    return fechasData.milisecActual < fechasData.milisecFechaRegistroMasHoras;
                });
                const noRespondidosArr = [];
                mensajesFiltroHora.forEach(dataMsg => {
                    noRespondidosArr.push(dataMsg.correlationId);
                });
                const setNoRespondidosArr = new Set(noRespondidosArr);
                const dataNoRepetida = noRespondidosArr.filter(onlyUnique);
                if (mensajes) {
                    const json = {
                        mensajes: dataNoRepetida,
                        cantidadNotificaciones: setNoRespondidosArr.size,
                        socketNumero: 3,
                    }; // AQUIIIIIIIIIII
                    req.body = {...req.body, ...json};
                    // sendSocket(req, res);
                    // IO.io.emit(`msg-noRespondido-${idEmpleado}`, {
                    //     mensajes: dataNoRepetida,
                    //     cantidadNotificaciones: setNoRespondidosArr.size
                    // });
                }
            } catch (e) {
                manejoErrores(e, res, 500);
            }

            function onlyUnique(value, index, self) {
                return self.indexOf(value) === index;
            }

            function timeConversion(fecha, fechaMasHora, dateMsec) {
                const msecPerMinute = 1000 * 60;
                const msecPerHour = msecPerMinute * 60;
                const msecPerDay = msecPerHour * 24;

                const milisecFechaRegistro = fecha / msecPerDay;
                const milisecFechaRegistroMasHoras = fechaMasHora / msecPerDay;
                const milisecActual = dateMsec / msecPerDay;

                return {
                    milisecFechaRegistro: milisecFechaRegistro,
                    milisecFechaRegistroMasHoras: milisecFechaRegistroMasHoras,
                    milisecActual: milisecActual
                }
            }
        }
    };
    static postCanalWhatsPlantilla = async (req: Request, res: Response) => {
        // número del origen (teléfono celular con clave de país "521")
        // let numero = req.body.data[0].origin;
        let numero = req.body.dataSend[0].origin;
        const { numeroGenerador, numerosRestantes } = req.body.dataNumerosWavy;

        // quita el 521 de los números para poder hacer la búsqueda en la tabla de solicitudes
        numero = numero.slice(3);

        // cantidad de mensajes
        const cantidadMensajes = req.body.dataSend.length;

        /* Si el req.body contiene idTipoEnvio, significa que es una respuesta del usuario (idTipoEnvio = 2), por el
        contrario, significa que es un mensaje saliente (idTipoEnvio = 1) */
        const tipoEnvio = await Object.keys(req.body).some(function(tipo) {
            return tipo === 'idTipoEnvio';
        });
        /*Si la funcion de arriba retorna un true, se le cambia el valor de la propiedad idEmpleadoEnvio a null porque
          el que mandó mensaje fue un MO. Caso controrio, a la propiedad idEmpleadoEnvio se le asigna el id del empleado
          que contestó dicho mensaje*/
        tipoEnvio ? req.body = {...req.body, 'idEmpleadoEnvio': null} : req.body = {...req.body, 'idEmpleadoEnvio': req.body.idEmpleadoEnvio}; // para saber que empleado manda mensaje

        const tipoMen = await Object.keys(req.body).some(function(tipoMsj) {
            return tipoMsj === 'idTipoMensajeWhatsapp';
        });

        // tipoMensaje ? req.body = {...req.body, 'idTipoMensajeWhatsapp': 2} : req.body = {...req.body, 'idTipoMensajeWhatsapp': req.body.idEmpleadoEnvio};

        const tipoMensajeWhatsappRepository = getRepository(TipoMensajeWhatsapp);
        const aux = [];
        let canalGuardado: any = {};

        try {
            // obtiene el id de la solicitud
            // const tipoMensaje: any = await tipoMensajeWhatsappRepository.findOneOrFail({
            //     where: {tipoMensaje: req.body.data[0].message.type}
            // }).catch(e => {
            //     manejoErrores('No se encontró el tipo de mensaje', e, 404);
            // });

            let cWhatsapp = new canalWhatsapp();
            cWhatsapp.idEstadoCanal = tipoEnvio ? 2 : 1;
            cWhatsapp.idSolicitud = req.body.dataSend[0].correlationId;
            cWhatsapp.respuestaMensajesMo = req.body.dataSend[0];
            cWhatsapp.cantidadMo = 1;
            cWhatsapp.destino = numero;
            cWhatsapp.fechaEnvio = new Date();
            cWhatsapp.origen = numeroGenerador;
            cWhatsapp.numerosRestantes = numerosRestantes;
            cWhatsapp.activacion = 1;

            canalGuardado = await getRepository(canalWhatsapp).save(cWhatsapp);

            let mensajesWha = new mensajesWhatsapp();
            mensajesWha.idCanalWhatsapp = canalGuardado.id;
            mensajesWha.idEmpleadoEnvio = req.body.idEmpleadoEnvio;
            mensajesWha.idTipoEnvio = tipoEnvio ? 2 : 1;
            mensajesWha.idTipoMensajeWhatsapp = tipoMen ? 2 : 1; //
            mensajesWha.mensaje = req.body.dataSend[0].message.messageText; // mensajes enviados por MO
            mensajesWha.fecha = new Date();
            mensajesWha.correlationId = req.body.dataSend[0].correlationId;
            mensajesWha.aBandeja = tipoEnvio ? 1 : 0;
            mensajesWha.idEmpleadoMensajes = req.body.idEmpleadoEnvio;

            const mensajesWhats = await getRepository(mensajesWhatsapp).save(mensajesWha);

            // manda mensaje privado
            let json = {};
            // console.log(`room_${req.body.data[0].correlationId}`, 'room_${solicitud.id}room_${solicitud.id}room_${solicitud.id}')
            // recorre el array de aux (el cual contienen los mensajes) para emitirlos de manera individual
            /*Dentro del json se encuentra toda la información de los mensajes más el idTipoEnvio*/
            json = {...mensajesWha, idTipoEnvio: mensajesWha.idTipoEnvio, mensaje: 'Plantilla enviada'};
            // console.log('esto va para: ',`room_${req.body.data[0].correlationId}`);
            /* identifica a qué socket tiene que emitir los mensajes que llegan (IO.io.to). room_${req.body.data[0].correlationId} es una
               sala que se maneja con el idSolicitud (req.body.data[0].correlationId) para mandar mensajes privados*/
            // IO.io.to(`room_${req.body.dataSend[0].correlationId}`).emit(`mensaje-nuevo${req.body.dataSend[0].correlationId}`, json);
            // se manda al front la información de las tablas canalWhatsapp y mensajesWhatsaoo
            res.send({canalWhatsapp: canalGuardado, mensajesWhatsapp: mensajesWhats});
            return;
        } catch (e) {
            res.status(400).send("request not found");
        }
    };
    /**
     * Se generan los registros en la tabla canalWhatsapp, mensajesWhatsapp **/
    static postCanalWhats = async (req: Request, res: Response) => {
        const idEmpleado = req.params.idEmpleado;

        const dStatus = await Object.keys(req.body).some(function(deliveredStatus) {
            return deliveredStatus === 'deliveredStatus';
        });

        const rParams = await Object.keys(req.params).some(function(idSolicitud) {
            return idSolicitud === 'idSolicitud';
        });

        await identificarTipoMensaje(req.body, rParams).then( async msg => {
            const mensajeTipos = msg;
            let jsonSolicitudes;
            let jsonHeader;

            let tipoMensajes = await rParams ? req.body.dataSend[0].message.type : req.body.data[0].message.type;
            const cantidadMensajes = rParams ? req.body.dataSend.length : req.body.data.length;
            const tipoEnvio = await Object.keys(req.body).some(function(tipo) {
                return tipo === 'idTipoEnvio';
            });

            // const idSolicitud = await dStatus ? req.body.data[0].correlationId : req.params.idSolicitud;
            const idSolicitud = rParams ? req.body.dataSend[0].correlationId : req.body.data[0].correlationId;

            const data = await rParams ? req.body.dataSend[0] : req.body.data[0];
            let numero = await rParams ? req.body.dataSend[0].origin : data.source;

            tipoEnvio ? req.body = {...req.body, 'idEmpleadoEnvio': null} : req.body = {...req.body, 'idEmpleadoEnvio': req.body.idEmpleadoEnvio}; // para saber que empleado manda mensaje

            const tipoMen = await Object.keys(req.body).some(function(tipoMsj) {
                return tipoMsj === 'idTipoMensajeWhatsapp';
            });

            const tipoMensajeWhatsappRepository = getRepository(TipoMensajeWhatsapp);
            const mensajesWhatsappRepository = getRepository(mensajesWhatsapp);
            const canalWhatsappRepository = getRepository(canalWhatsapp);
            const aux = [];
            let canalGuardado: any = {};
            // solo la propiedad message. Se extraen los mensajes y se guardan en la variable aux
            for (let i = 0; i < cantidadMensajes; i++) {
                aux.push(mensajeTipos);
            }

            try {
                const tipoMensaje: any = await tipoMensajeWhatsappRepository.findOneOrFail({
                    where: {tipoMensaje: tipoMensajes}
                }).catch(e => {
                    manejoErrores('No se encontró el tipo de mensaje', res, 404, e);
                });
                const whatsMessages: any = await mensajesWhatsappRepository.find({
                    where: {correlationId: idSolicitud}
                }).catch(e => {
                    manejoErrores('No se encontró ningún mensaje/s', res, 404, e);
                });
                const canalWhatsMessages: any = await canalWhatsappRepository.findOne({
                    where: {idSolicitud: idSolicitud}
                }).catch(e => {
                    manejoErrores(`No se encontró ningún canal con el idSolicitud ${req.body.data[0].correlationId}`, res, 404, e);
                });

                let cWhatsapp = new canalWhatsapp();
                cWhatsapp.idEstadoCanal = tipoEnvio ? 2 : 1;
                cWhatsapp.idSolicitud = idSolicitud;
                cWhatsapp.respuestaMensajesMo = rParams ? req.body.dataSend : req.body.data;
                cWhatsapp.cantidadMo = cantidadMensajes;
                cWhatsapp.destino = numero;
                cWhatsapp.activacion = 1;

                if (canalWhatsMessages) {
                    canalGuardado = canalWhatsMessages;
                    let mensajesWha = new mensajesWhatsapp();
                    mensajesWha.idCanalWhatsapp = canalGuardado.id;
                    mensajesWha.idEmpleadoEnvio = req.body.idEmpleadoEnvio;
                    mensajesWha.idTipoEnvio = tipoEnvio ? 2 : 1;
                    mensajesWha.idTipoMensajeWhatsapp = tipoMen ? 2 : tipoMensaje.id;
                    mensajesWha.mensaje = mensajeTipos; // mensajes enviados por MO
                    mensajesWha.fecha = new Date();
                    mensajesWha.correlationId = rParams ? req.body.dataSend[0].correlationId : req.body.data[0].correlationId;
                    mensajesWha.aBandeja = tipoEnvio ? 1 : 0;
                    mensajesWha.idEmpleadoMensajes = idEmpleado;
                    // asigna el canal a los mensajes
                    if (whatsMessages && whatsMessages.length > 0) {
                        mensajesWha.idCanalWhatsapp = whatsMessages[0].idCanalWhatsapp;
                    }
                    const mensajesWhats = await getRepository(mensajesWhatsapp).save(mensajesWha);

                    // manda mensaje privado
                    let json;
                    // recorre el array de aux (el cual contienen los mensajes) para emitirlos de manera individual
                    // for (let i = 0; i < aux.length; i++) {
                    const jsonCantidadMensajesIndividuales = await counter(idEmpleado);
                    const solicitudDestino = rParams ? req.body.dataSend[0].correlationId : req.body.data[0].correlationId;
                    json = {
                        ...mensajesWha,
                        idTipoEnvio: mensajesWha.idTipoEnvio,
                        mensaje: mensajeTipos,
                        tipoMensaje: tipoMensajes,
                        notificacion: 1,
                        cantidadMensajesIndividuales: jsonCantidadMensajesIndividuales,
                        socketNumero: 4,
                        solicitudDestino: solicitudDestino,
                    }; // AQUIIIIIIIIIII

                    const correlationId = rParams ? req.body.dataSend[0].correlationId : req.body.data[0].correlationId;
                    // if (tipoRoom === 'chat') {
                    //     IO.io.emit(`mensaje-nuevo${correlationId}`, json);
                    // }
                    sendSocket({
                        ...mensajesWha,
                        idTipoEnvio: mensajesWha.idTipoEnvio,
                        mensaje: mensajeTipos,
                        tipoMensaje: tipoMensajes,
                        notificacion: 1,
                        cantidadMensajesIndividuales: jsonCantidadMensajesIndividuales,
                        socketNumero: 4,
                        solicitudDestino: solicitudDestino,
                        idEmpleado: idEmpleado,
                        socket: 'chat',
                    }, res);
                    req.body = {...req.body, ...json};
                    // sendSocket(req, res);
                    if (mensajesWhats.idTipoEnvio === 2) {
                        const solicitudDestino = rParams ? req.body.dataSend[0].correlationId : req.body.data[0].correlationId;
                        jsonSolicitudes = {
                            ...mensajesWha,
                            idTipoEnvio: mensajesWha.idTipoEnvio,
                            mensaje: mensajeTipos,
                            tipoMensaje: tipoMensajes,
                            notificacion: 1,
                            cantidadMensajesIndividuales: jsonCantidadMensajesIndividuales,
                            socketNumero: 4,
                            solicitudDestino: solicitudDestino,
                        }; // AQUIIIIIIIIIII
                        req.body = {...req.body, ...jsonSolicitudes};
                        // sendSocket(req, res);
                        // if (tipoRoom === 'solicitudes') {
                        //     IO.io.emit(`msg-solicitudes-${idEmpleado}`, json);
                        // }
                        sendSocket({
                            ...mensajesWha,
                            idTipoEnvio: mensajesWha.idTipoEnvio,
                            mensaje: mensajeTipos,
                            tipoMensaje: tipoMensajes,
                            notificacion: 1,
                            cantidadMensajesIndividuales: jsonCantidadMensajesIndividuales,
                            solicitudDestino: solicitudDestino,
                            idEmpleado: idEmpleado,
                            socket: 'solicitudes',
                        }, res);
                        req.body = {...req.body, ...json};
                        // ***********************************************************************************************************************************************
                        const mensajesWhatsappRepository = getRepository(mensajesWhatsapp);

                        try {
                            const mensajes = await mensajesWhatsappRepository.find({
                                where: {aBandeja: 1, idTipoEnvio: 2, idEmpleadoMensajes: idEmpleado}
                            });
                            const mensajesFiltroHora = mensajes.filter(data => {
                                const date = new Date(data.fecha);
                                const dateConHoras = new Date(data.fecha).setHours(24);
                                const dateActual = new Date();
                                const dateMsecActual = dateActual.getTime();

                                const fechasData = timeConversion(date, dateConHoras, dateMsecActual);
                                return fechasData.milisecActual < fechasData.milisecFechaRegistroMasHoras;
                            });
                            const noRespondidosArr = [];
                            mensajesFiltroHora.forEach(dataMsg => {
                                noRespondidosArr.push(dataMsg.correlationId);
                            });
                            const setNoRespondidosArr = new Set(noRespondidosArr);
                            const dataNoRepetida = noRespondidosArr.filter(onlyUnique);
                            if (mensajes) {
                                const solicitudDestino = rParams ? req.body.dataSend[0].correlationId : req.body.data[0].correlationId;
                                jsonHeader = {
                                    mensajes: dataNoRepetida,
                                    cantidadNotificaciones: setNoRespondidosArr.size,
                                    cantidadMensajesIndividuales: jsonCantidadMensajesIndividuales,
                                    socketNumero: 3,
                                    solicitudDestino: solicitudDestino,
                                }; // AQUIIIIIIIIIII
                                req.body = {...req.body, ...jsonHeader};
                                // sendSocket(req, res);
                                // if (tipoRoom === 'header') {
                                //     IO.io.emit(`msg-noRespondido-${idEmpleado}`, {
                                //         mensajes: dataNoRepetida,
                                //         cantidadNotificaciones: setNoRespondidosArr.size,
                                //         cantidadMensajesIndividuales: jsonCantidadMensajesIndividuales,
                                //     });
                                // }
                                sendSocket({
                                    mensajes: dataNoRepetida,
                                    cantidadNotificaciones: setNoRespondidosArr.size,
                                    cantidadMensajesIndividuales: jsonCantidadMensajesIndividuales,
                                    idEmpleado: idEmpleado,
                                    socket: 'header',
                                }, res);
                                sendSocket({
                                    mensajes: dataNoRepetida,
                                    cantidadNotificaciones: setNoRespondidosArr.size,
                                    cantidadMensajesIndividuales: jsonCantidadMensajesIndividuales,
                                    idEmpleado: idEmpleado,
                                    socket: 'headerSolicitudes',
                                }, res);
                                /*if (tipoRoom === 'header') {
                                    IO.io.emit(`msg-noRespondido-${idEmpleado}`, {
                                        mensajes: dataNoRepetida,
                                        cantidadNotificaciones: setNoRespondidosArr.size,
                                        cantidadMensajesIndividuales: jsonCantidadMensajesIndividuales,
                                    });
                                }

                                if (tipoRoom === 'headerSolicitudes') {
                                    IO.io.emit(`msg-noRespondido-${idEmpleado}`, {
                                        mensajes: dataNoRepetida,
                                        cantidadNotificaciones: setNoRespondidosArr.size,
                                        cantidadMensajesIndividuales: jsonCantidadMensajesIndividuales,
                                    });
                                    IO.io.emit(`msg-solicitudes-${idEmpleado}`, jsonSolicitudes);
                                }*/
                            }
                        } catch (e) {
                            manejoErrores(e, res, 500);
                        }
                        res.send({canalWhatsapp: canalGuardado, mensajesWhatsapp: mensajesWhats});
                        return;
                    }
                    // }
                    // se manda al front la información de las tablas canalWhatsapp y mensajesWhatsaoo
                    res.send({canalWhatsapp: canalGuardado, mensajesWhatsapp: mensajesWhats});
                    return;
                } else {
                    // const tipoRoomFisrt = tipoRoom + 'Canal';
                    canalGuardado = await getRepository(canalWhatsapp).save(cWhatsapp);

                    let mensajesWha = new mensajesWhatsapp();
                    mensajesWha.idCanalWhatsapp = canalGuardado.id;
                    mensajesWha.idEmpleadoEnvio = req.body.idEmpleadoEnvio;
                    mensajesWha.idTipoEnvio = tipoEnvio ? 2 : 1;
                    mensajesWha.idTipoMensajeWhatsapp = tipoMensaje.id;
                    mensajesWha.mensaje = mensajeTipos; // mensajes enviados por MO
                    mensajesWha.fecha = new Date();
                    mensajesWha.correlationId = rParams ? req.body.dataSend[0].correlationId : req.body.data[0].correlationId;
                    mensajesWha.aBandeja = tipoEnvio ? 1 : 0;
                    mensajesWha.idEmpleadoMensajes = idEmpleado;

                    if (whatsMessages && whatsMessages.length > 0) {
                        mensajesWha.idCanalWhatsapp = whatsMessages[0].idCanalWhatsapp;
                    }

                    const mensajesWhats = await getRepository(mensajesWhatsapp).save(mensajesWha);

                    // manda mensaje privado
                    let json;
                    // recorre el array de aux (el cual contienen los mensajes) para emitirlos de manera individual
                    // for (let i = 0; i < aux.length; i++) {
                    const jsonCantidadMensajesIndividuales = await counter(idEmpleado);
                    const correlationId = rParams ? req.body.dataSend[0].correlationId : req.body.data[0].correlationId;
                    json = {...mensajesWha, idTipoEnvio: mensajesWha.idTipoEnvio, mensaje: mensajeTipos, tipoMensaje: tipoMensajes, notificacion: 1, cantidadMensajesIndividuales: jsonCantidadMensajesIndividuales}; // AQUIIIIIIIIIII
                    // if (tipoRoomFisrt === 'chatCanal') {
                    //     IO.io.emit(`mensaje-nuevo${correlationId}`, json);
                    // }
                    sendSocket({
                        ...mensajesWha,
                        idTipoEnvio: mensajesWha.idTipoEnvio,
                        mensaje: mensajeTipos,
                        tipoMensaje: tipoMensajes,
                        notificacion: 1,
                        cantidadMensajesIndividuales: jsonCantidadMensajesIndividuales,
                        correlationId: correlationId,
                        socket: 'chatCanal',
                    }, res);
                    const solicitudDestino = rParams ? req.body.dataSend[0].correlationId : req.body.data[0].correlationId;
                    json = {
                        ...mensajesWha,
                        idTipoEnvio: mensajesWha.idTipoEnvio,
                        mensaje: mensajeTipos,
                        tipoMensaje: tipoMensajes,
                        notificacion: 1,
                        cantidadMensajesIndividuales: jsonCantidadMensajesIndividuales,
                        socketNumero: 4,
                        solicitudDestino: solicitudDestino,
                    }; // AQUIIIIIIIIIII
                    req.body = {...req.body, ...json};
                    // sendSocket(req, res);

                    if (mensajesWhats.idTipoEnvio === 2) {
                        // if (tipoRoomFisrt === 'solicitudesCanal') {
                        //     IO.io.emit(`msg-solicitudes-${idEmpleado}`, json);
                        // }
                        sendSocket({
                            ...mensajesWha,
                            idTipoEnvio: mensajesWha.idTipoEnvio,
                            mensaje: mensajeTipos,
                            tipoMensaje: tipoMensajes,
                            notificacion: 1,
                            cantidadMensajesIndividuales: jsonCantidadMensajesIndividuales,
                            solicitudDestino: solicitudDestino,
                            idEmpleado: idEmpleado,
                            socket: 'solicitudesCanal',
                        }, res);
                        req.body = {...req.body, ...json};
                        // sendSocket(req, res);
                        // ************************************************************************************************************************************************
                        const mensajesWhatsappRepository = getRepository(mensajesWhatsapp);

                        try {
                            const mensajes = await mensajesWhatsappRepository.find({
                                where: {aBandeja: 1, idTipoEnvio: 2}
                            });
                            const mensajesFiltroHora = mensajes.filter(data => {
                                const date = new Date(data.fecha);
                                const dateConHoras = new Date(data.fecha).setHours(24);
                                const dateActual = new Date();
                                const dateMsecActual = dateActual.getTime();

                                const fechasData = timeConversion(date, dateConHoras, dateMsecActual);
                                return fechasData.milisecActual < fechasData.milisecFechaRegistroMasHoras;
                            });
                            const noRespondidosArr = [];
                            mensajesFiltroHora.forEach(dataMsg => {
                                noRespondidosArr.push(dataMsg.correlationId);
                            });
                            const setNoRespondidosArr = new Set(noRespondidosArr);
                            const dataNoRepetida = noRespondidosArr.filter(onlyUnique);
                            if (mensajes) {
                                const solicitudDestino = rParams ? req.body.dataSend[0].correlationId : req.body.data[0].correlationId;
                                json = {
                                    mensajes: dataNoRepetida,
                                    cantidadNotificaciones: setNoRespondidosArr.size,
                                    cantidadMensajesIndividuales: jsonCantidadMensajesIndividuales,
                                    socketNumero: 3,
                                    solicitudDestino: solicitudDestino,
                                }; // AQUIIIIIIIIIII
                                req.body = {...req.body, ...json};
                                // sendSocket(req, res);
                                // if (tipoRoomFisrt === 'headerCanal') {
                                //     IO.io.emit(`msg-noRespondido-${idEmpleado}`, {
                                //         mensajes: dataNoRepetida,
                                //         cantidadNotificaciones: setNoRespondidosArr.size,
                                //         cantidadMensajesIndividuales: jsonCantidadMensajesIndividuales,
                                //     });
                                // }
                                sendSocket({
                                    mensajes: dataNoRepetida,
                                    cantidadNotificaciones: setNoRespondidosArr.size,
                                    cantidadMensajesIndividuales: jsonCantidadMensajesIndividuales,
                                    idEmpleado: idEmpleado,
                                    socket: 'headerCanal',
                                }, res);
                            }
                        } catch (e) {
                            manejoErrores(e, res, 500);
                        }
                        // res.send({canalWhatsapp: canalGuardado, mensajesWhatsapp: mensajesWhats});
                    }
                    // }
                    // se manda al front la información de las tablas canalWhatsapp y mensajesWhatsaoo
                    res.send({canalWhatsapp: canalGuardado, mensajesWhatsapp: mensajesWhats});
                }
            } catch (e) {
                res.send(e);
            }
        });

        function identificarTipoMensaje(reeq, flag) {
            return new Promise( function (resolve, reject) {
                if (flag) {
                    switch (reeq.dataSend[0].message.type) {
                        case "TEXT":
                            resolve(reeq.dataSend[0].message.messageText);
                            break;

                        case "IMAGE":
                            resolve(reeq.dataSend[0].message.mediaUrl);
                            break;

                        case "AUDIO":
                            resolve(reeq.dataSend[0].message.mediaUrl);
                            break;

                        case "LOCATION":
                            resolve(reeq.dataSend[0].message.location.geoPoint);
                            break;

                        case "DOCUMENT":
                            resolve(reeq.dataSend[0].message.mediaUrl);
                        break;

                        case "STICKER":
                            resolve(reeq.dataSend[0].message.mediaUrl);
                        break;

                        default:
                            reject('ERROR');
                    }
                } else {
                    switch (reeq.data[0].message.type) {
                        case "TEXT":
                            resolve(reeq.data[0].message.messageText);
                            break;

                        case "IMAGE":
                            resolve(reeq.data[0].message.mediaUrl);
                            break;

                        case "AUDIO":
                            resolve(reeq.data[0].message.mediaUrl);
                            break;

                        case "LOCATION":
                            resolve(reeq.data[0].message.location.geoPoint);
                            break;

                        case "DOCUMENT":
                            resolve(reeq.data[0].message.mediaUrl);
                            break;

                        case "STICKER":
                            resolve(reeq.data[0].message.mediaUrl);
                        break;

                        default:
                            reject('ERROR');
                    }
                }
            })
        }

        function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
        }

        function timeConversion(fecha, fechaMasHora, dateMsec) {
            const msecPerMinute = 1000 * 60;
            const msecPerHour = msecPerMinute * 60;
            const msecPerDay = msecPerHour * 24;

            const milisecFechaRegistro = fecha / msecPerDay;
            const milisecFechaRegistroMasHoras = fechaMasHora / msecPerDay;
            const milisecActual = dateMsec / msecPerDay;

            return {
                milisecFechaRegistro: milisecFechaRegistro,
                milisecFechaRegistroMasHoras: milisecFechaRegistroMasHoras,
                milisecActual: milisecActual
            }
        }

        // async function getMensajesAndCanal(send) {
        //
        //     const mensajesWhatsappRepository = getRepository(mensajesWhatsapp);
        //
        //     try {
        //         const mensajes = await mensajesWhatsappRepository.find({
        //             where: {aBandeja: 1, idTipoEnvio: 2}
        //         });
        //         const mensajesFiltroHora = mensajes.filter(data => {
        //             const date = new Date(data.fecha);
        //             const dateConHoras = new Date(data.fecha).setHours(24);
        //             const dateActual = new Date();
        //             const dateMsecActual = dateActual.getTime();
        //
        //             const fechasData = timeConversion(date, dateConHoras, dateMsecActual);
        //             return fechasData.milisecActual < fechasData.milisecFechaRegistroMasHoras;
        //         });
        //         const noRespondidosArr = [];
        //         mensajesFiltroHora.forEach(dataMsg => {
        //             noRespondidosArr.push(dataMsg.correlationId);
        //         });
        //         const setNoRespondidosArr = new Set(noRespondidosArr);
        //         const dataNoRepetida = noRespondidosArr.filter(onlyUnique);
        //         if (mensajes) {
        //             IO.io.to(`room_solicitudes`).emit(`msg-noRespondido`, {
        //                 mensajes: dataNoRepetida,
        //                 cantidadNotificaciones: setNoRespondidosArr.size
        //             });
        //             res.send(send);
        //         }
        //     } catch (e) {
        //         manejoErrores(e, res, 500);
        //     }
        //
        //     function onlyUnique(value, index, self) {
        //         return self.indexOf(value) === index;
        //     }
        //
        //     function timeConversion(fecha, fechaMasHora, dateMsec) {
        //         const msecPerMinute = 1000 * 60;
        //         const msecPerHour = msecPerMinute * 60;
        //         const msecPerDay = msecPerHour * 24;
        //
        //         const milisecFechaRegistro = fecha / msecPerDay;
        //         const milisecFechaRegistroMasHoras = fechaMasHora / msecPerDay;
        //         const milisecActual = dateMsec / msecPerDay;
        //
        //         return {
        //             milisecFechaRegistro: milisecFechaRegistro,
        //             milisecFechaRegistroMasHoras: milisecFechaRegistroMasHoras,
        //             milisecActual: milisecActual
        //         }
        //     }
        // }

    };

    static postMensajePorReactivacionDeCanal = async (req: Request, res: Response) => {
        const mensajeData = req.body;
        try {
            let mensajesWha = new mensajesWhatsapp();
            mensajesWha.idCanalWhatsapp = mensajeData.idCanalWhatsapp;
            mensajesWha.idEmpleadoEnvio = req.body.idEmpleadoEnvio;
            mensajesWha.idTipoEnvio = 1;
            mensajesWha.idTipoMensajeWhatsapp = 1; //
            mensajesWha.mensaje = mensajeData.mensaje; // mensajes enviados por MO
            mensajesWha.fecha = new Date();
            mensajesWha.correlationId = req.body.correlationId;
            mensajesWha.aBandeja = 0;
            mensajesWha.idEmpleadoMensajes = req.body.idEmpleadoEnvio;

            const mensajesWhats = await getRepository(mensajesWhatsapp).save(mensajesWha);

            // manda mensaje privado
            let json = {};
            // console.log(`room_${req.body.data[0].correlationId}`, 'room_${solicitud.id}room_${solicitud.id}room_${solicitud.id}')
            // recorre el array de aux (el cual contienen los mensajes) para emitirlos de manera individual
            /*Dentro del json se encuentra toda la información de los mensajes más el idTipoEnvio*/
            json = {...mensajesWha, idTipoEnvio: mensajesWha.idTipoEnvio, mensaje: 'Plantilla enviada'};
            // console.log('esto va para: ',`room_${req.body.data[0].correlationId}`);
            /* identifica a qué socket tiene que emitir los mensajes que llegan (IO.io.to). room_${req.body.data[0].correlationId} es una
               sala que se maneja con el idSolicitud (req.body.data[0].correlationId) para mandar mensajes privados*/
            // IO.io.to(`room_${req.body.correlationId}`).emit(`mensaje-nuevo${req.body.correlationId}`, json);
            // se manda al front la información de las tablas canalWhatsapp y mensajesWhatsaoo
            res.send({mensajesWhatsapp: mensajesWhats});
            return;
        } catch (e) {
            // console.log(e)
            // res.status(400).send("request not found");
            manejoErrores(`No se reactivó el canal`, res, 404, e);
        }
    };

    static testSocketSolicitudes = async (req: Request, res: Response) => {
        const json = {
            correlationId: '157471',
        };
        // IO.io.to(`room_solicitudes`).emit(`msg-solicitudes`, json);
    }

    /*
    * Verifica si ya hay un canal existente, para que no se pueda abrir la misma conversacion
    * dos veces*/
    static chatTwice = async (req: Request, res: Response) => {
        const numero = req.params.numero;
        const idCotizacionAli = req.params.idCotizacionAli;

        const canalWhatsappRepository = getRepository(canalWhatsapp);
        const cotizacionAliRepository = getRepository(CotizacionesAli);
        try {
            const canalEncontrado = await canalWhatsappRepository.find({
                where: {destino: numero}
            });
            const cotizacionAliData: any = await cotizacionAliRepository.find({
                where: {id: idCotizacionAli}
            });

            const peticion = cotizacionAliData[0].peticion;

            if (canalEncontrado.length !== 0) {
                res.send({
                    found: 1,
                    data: canalEncontrado,
                    dataParametros: {
                        marca: peticion.marca,
                        modelo: peticion.modelo
                    }
                    // solicitud: canalEncontrado.idSolicitud,
                    // destino: canalEncontrado.destino,
                    // idCanal: canalEncontrado.id,
                    // numeroOrigen: canalEncontrado.origen,
                    // numerosRestantes: canalEncontrado.numerosRestantes
                });
            } else {
                res.send({found: 0, dataParametros: {
                        marca: peticion.marca,
                        modelo: peticion.modelo
                    }});
            }
        } catch (e) {
            manejoErrores(`Canal no encontrado, parámetros numero: ${numero}, idCotizacionAli: ${idCotizacionAli}`, res, 404, e);
            // res.status(500);
        }
    };

    static chatTwiceOrigenDestino = async (req: Request, res: Response) => {
        const numeroDestino = req.params.numeroDestino;
        const numeroOrigen = req.params.numeroOrigen;

        const canalWhatsappRepository = getRepository(canalWhatsapp);
        try {
            const canalEncontrado = await canalWhatsappRepository.findOne({
                where: {
                    destino: numeroDestino,
                    origen: numeroOrigen
                }
            }).catch(e => {
                manejoErrores(`Canal no encontrado, parámetros ${numeroDestino}, ${numeroOrigen}`, res, 404, e);
            });

            if (canalEncontrado) {
                res.send({
                    // data: canalEncontrado
                    found: 1,
                    solicitud: canalEncontrado.idSolicitud,
                    destino: canalEncontrado.destino,
                    idCanal: canalEncontrado.id,
                    numeroOrigen: canalEncontrado.origen,
                    numerosRestantes: canalEncontrado.numerosRestantes
                });
            } else {
                res.send({found: 0});
            }
        } catch (e) {
            res.status(500);
        }
    };

    static getCatalogoNumeroWavy = async (req: Request, res: Response) => {

        const catalogoNumerosWavyRepository = getRepository(CatalogoNumerosWavy);

        try {
            const catalogoNumerosWavyData = await catalogoNumerosWavyRepository.find({
                where: {idMarca: 1}                 // LA MARCA ES AHORRA SEGUROS
            }).catch(e => {
                manejoErrores('No se encontró datos en el catálogo', e, 404);
            });
            res.status(200).json(catalogoNumerosWavyData);
        } catch (e) {
            res.status(500).send("request not found");
        }
    };

    static activarCanalPorSegundaPlantilla = async (req: Request, res: Response) => {
        const id = req.body.id;
        const dataCanal = req.body;

        const dataCanalRes =  {
            ...dataCanal,
            activacion: 1
        };
        try {
            await getRepository(canalWhatsapp).update(id, dataCanalRes).then(() => {
                res.status(200).json('Se actualizaron los datos');
            });
        } catch (e) {
            console.log(e, 'eeeeeee')
            res.status(500).json({error: e});
        }
    };

    static getMensajesRespondidos = async (req: Request, res: Response) => {
        const idEmpleado = req.params.idEmpleado;
        const mensajesWhatsappRepository = getRepository(mensajesWhatsapp);

        try {
            const mensajes = await mensajesWhatsappRepository.find({
                where: {aBandeja: 0, idTipoEnvio: 1, idEmpleadoMensajes: idEmpleado}
            });
            // res.send({mensajes: mensajes});
            const mensajesFiltroHora = mensajes.filter( data => {
                const date = new Date(data.fecha);
                const dateConHoras = new Date(data.fecha).setHours(24);
                const dateActual = new Date();
                const dateMsecActual = dateActual.getTime();

                const fechasData = timeConversion(date, dateConHoras, dateMsecActual);
                return fechasData.milisecActual < fechasData.milisecFechaRegistroMasHoras;
            });
            const respondidosArr = [];
            mensajesFiltroHora.forEach(dataMsg => {
                respondidosArr.push(dataMsg.correlationId);
            });
            /* cuenta cuantos mensajes tienen por responder cada canal
             { '161552': 1, '161553': 1, '161555': 2 } */
            const respondidosArrSorted = respondidosArr.sort();
            let counter = {};
            for(let i = 0; i < respondidosArrSorted.length; i++) {
                if(!(respondidosArrSorted[i] in counter))counter[respondidosArrSorted[i]] = 0;
                counter[respondidosArrSorted[i]]++;
            } /////////////////////////////////////--------------
            // **********************************************************************************************************
            const setRespondidosArr = new Set(respondidosArr);

            const dataNoRepetida = respondidosArr.filter( onlyUnique );

            // pendiente, para poder sacar la fecha y mostrarla en la bandeja
            const paraFechaArr = [];
            const dataClasificada = dataNoRepetida.map(d => {
                return mensajesFiltroHora.filter(val => {
                    return val.correlationId === d
                });
            });
            for (let i = 0; i < dataClasificada.length; i++) {
                for (let j = 0; j < dataClasificada[i].length; j++) {
                    if (dataClasificada[i].length > 1 ) {
                        paraFechaArr.push(dataClasificada[dataClasificada.length - 1]);
                    } else {
                        paraFechaArr.push(dataClasificada[0]);
                    }
                }
            }
            // ********************************************************************
            if (mensajes) {
                const json = {
                    mensajes: dataNoRepetida,
                    cantidadNotificaciones: setRespondidosArr.size,
                    socketNumero: 3,
                }; // AQUIIIIIIIIIII
                req.body = {...req.body, ...json};
                // sendSocket(req, res);
                // IO.io.to(`room_solicitudes_${idEmpleado}`).emit(`msg-noRespondido-${idEmpleado}`, {
                //     mensajes: dataNoRepetida,
                //     cantidadNotificaciones: setNoRespondidosArr.size,
                //     cantidadMensajesIndividuales: counter,
                // });
                res.send({mensajes: dataNoRepetida, cantidadNotificaciones: setRespondidosArr.size, cantidadMensajesIndividuales: counter, dataClasificada: dataClasificada});
            }
        } catch (e) {
            manejoErrores(e, res, 500);
        }

        function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
        }

        function timeConversion(fecha, fechaMasHora, dateMsec) {
            const msecPerMinute = 1000 * 60;
            const msecPerHour = msecPerMinute * 60;
            const msecPerDay = msecPerHour * 24;

            const milisecFechaRegistro = fecha / msecPerDay;
            const milisecFechaRegistroMasHoras = fechaMasHora / msecPerDay;
            const milisecActual = dateMsec / msecPerDay;

            return {
                milisecFechaRegistro: milisecFechaRegistro,
                milisecFechaRegistroMasHoras: milisecFechaRegistroMasHoras,
                milisecActual: milisecActual
            }
        }
    };

    static getMensajesDeEjecutivosPorSupervisor = async (req: Request, res: Response) => {
        const idEmpleado = req.query.idEmpleado;
        const idPuesto = req.query.idPuesto;
        const idSubarea = req.query.idSubarea;
        const fechaInicio = req.query.fechaInicio;
        const fechaFin = req.query.fechaFin;

        const empleadoRepository = getRepository(empleadoView);
        const supervisoresTipoSubareaRepository = await getRepository(SupervisoresSubareaWhatsappView).manager.query(`SELECT * FROM supervisoresSubareaWhatsappView WHERE idEmpleado = ${idEmpleado};`);
        const mensajesWhatappRepository = getRepository(mensajesWhatsapp);
        const mensajesWhatsappData = await mensajesWhatappRepository.find();

        try {
            const empleadoSupervisorData: any = await empleadoRepository.findOneOrFail({
                where: {
                    id: +idEmpleado,
                    idPuesto: +idPuesto,
                    idSubarea: +idSubarea
                }
            }).catch(e => {
                console.log(e, 'errooooor 2 /mensajes-de-ejecutivos-por-supervisor-fecha');
                manejoErrores('No se encontró el idEmpleado', e, 404);
            });

            let counterSubareaName = {};

            const idTipoSubareaSupervisor = empleadoSupervisorData.idTipoSubarea;
            const supervisoresTipoSubareaRepositorySinDuplicidad = removeDuplicates(supervisoresTipoSubareaRepository, 'idTipoSubarea');
            for (let h = 0; h < supervisoresTipoSubareaRepositorySinDuplicidad.length; h++) {
                const empleadosMensajesWhatsappViewRepository = getRepository(EmpleadosMensajesWhatsappView).manager.query(`SELECT * FROM empleadosMensajesWhatsappView WHERE idTipoSubarea = ${supervisoresTipoSubareaRepositorySinDuplicidad[h].idTipoSubarea} AND fecha BETWEEN '${fechaInicio}' AND '${fechaFin}';`);

                const dataMensajes = await empleadosMensajesWhatsappViewRepository.then(data => {
                    return data;
                }).catch(e => {
                    console.log(e, 'error en dataMensajes')
                });
                const idEmpleadoArr = [];
                dataMensajes.forEach(dataMsg => {
                    idEmpleadoArr.push({nombre: dataMsg.nombre + ' ' + dataMsg.paterno + ' ' + dataMsg.materno, idCanalWhatsapp: +dataMsg.idCanalWhatsapp, idEmpleado: +dataMsg.idEmpleado, idSolicitud: +dataMsg.correlationId, fecha: dataMsg.fecha, subarea: dataMsg.subarea, idSubarea: dataMsg.idSubarea});
                });

                // cuenta cuantos mensajes tienen por responder cada canal
                // { '161552': 1, '161553': 1, '161555': 2 }
                // const idEmpleadoArrArrSorted = idEmpleadoArr.sort();

                let counterIdSolicitud = {};
                const arrAuxIdSolicitud = [];

                // let counterSubareaName = {};

                /*for(let i = 0; i < idEmpleadoArr.length; i++) {

                    if(!(idEmpleadoArr[i].nombre in counter)) {
                        idEmpleadoArr.forEach(data => {
                            if (data.nombre === idEmpleadoArr[i].nombre) {
                                // arrAux.push(data.idCanalWhatsapp);
                                arrAux.push(data);
                            }
                        });

                        // counter[idEmpleadoArr[i].nombre] = arrAux;
                        let arrr = [];
                        const ret = arrAux.filter(res => {
                            return res.nombre === idEmpleadoArr[i].nombre;
                        });
                        for (let i = 0; i < ret.length; i++) {
                            arrr.push(ret[i].idCanalWhatsapp);
                        }
                        counter[idEmpleadoArr[i].nombre] = Array.from(new Set(arrr));
                    }
                    counter[idEmpleadoArr[i].nombre];
                }*/

                const arrSubareaName = [];
                supervisoresTipoSubareaRepository.forEach(dataSupervisiores => {
                    arrSubareaName.push({subarea: dataSupervisiores.subarea});
                });

                const arrAuxSubareaName = [];



                for(let k = 0; k < arrSubareaName.length; k++) {
                    if(!(arrSubareaName[k].subarea in counterSubareaName)) {
                        arrSubareaName.forEach(data => {
                            if (data.subarea === arrSubareaName[k].subarea) { // se compararía con la propiedad subarea de los mesajes
                                arrAuxSubareaName.push(data);
                                counterSubareaName[arrAuxSubareaName[k].subarea] = {};
                            }
                        });
                    }
                    // hasta aquí sale así
                    // counterSubareaName[arrAuxSubareaName[i].subarea] = arrAuxSubareaName;
                    /*
                    * {
                         'VN AHORRA SEGUROS': [ { subarea: 'VN AHORRA SEGUROS' }, { subarea: 'VN AHORRA OUT' } ],
                         'VN AHORRA OUT': [ { subarea: 'VN AHORRA SEGUROS' }, { subarea: 'VN AHORRA OUT' } ]
                      }
                    * */
                }


                for(let i = 0; i < idEmpleadoArr.length; i++) {
                    if(!(idEmpleadoArr[i].nombre in counterIdSolicitud)) {
                        idEmpleadoArr.forEach(data => {
                            if (data.nombre === idEmpleadoArr[i].nombre) {
                                // arrAux.push(data.idCanalWhatsapp);
                                arrAuxIdSolicitud.push(data);
                            }
                        });

                        // counter[idEmpleadoArr[i].nombre] = arrAux;
                        let arrrIdSolicitud = [];
                        const retIdSolicitud = arrAuxIdSolicitud.filter(res => {
                            return res.nombre === idEmpleadoArr[i].nombre;
                        });

                        for (let i = 0; i < retIdSolicitud.length; i++) {
                            // arrrIdSolicitud.push({idSolicitud: retIdSolicitud[i].idSolicitud, fechaCreacion: retIdSolicitud[i].fecha});
                            const prueba: any = {};
                            prueba[+retIdSolicitud[i].idSolicitud] = mensajesWhatsappData.filter(res => {
                                return res.correlationId === +retIdSolicitud[i].idSolicitud;
                            });
                            const responderData = mensajesWhatsappData.filter(res => {
                                return res.correlationId === +retIdSolicitud[i].idSolicitud;
                            });
                            prueba['subarea'] = retIdSolicitud[i].subarea;
                            prueba['idSubarea'] = retIdSolicitud[i].idSubarea;
                            prueba['fechaCreacion'] = retIdSolicitud[i].fecha;
                            prueba['idSolicitud'] = +retIdSolicitud[i].idSolicitud;
                            prueba['responder'] = responderData.length === 1 ? 0 : responderData[responderData.length - 1].idTipoEnvio === 2;
                            arrrIdSolicitud.push(prueba);
                            // console.log(arrrIdSolicitud, 'arrrIdSolicitudarrrIdSolicitudarrrIdSolicitudarrrIdSolicitud')
                            arrrIdSolicitud = removeDuplicates(arrrIdSolicitud, retIdSolicitud[i].idSolicitud);
                        }

                        counterIdSolicitud[idEmpleadoArr[i].nombre] = [arrrIdSolicitud[0]['subarea'], ...arrrIdSolicitud];

                        let claves = Object.keys(counterIdSolicitud);
                        let clavesSubarea = Object.keys(counterSubareaName);
                        for(let i=0; i< claves.length; i++){
                            let clave = claves[i];
                            for(let j=0; j< clavesSubarea.length; j++){
                                let claveSubarea = clavesSubarea[j];
                                if (counterIdSolicitud[clave][0] === clavesSubarea[j]) {
                                    counterSubareaName[clavesSubarea[j]] = counterIdSolicitud;
                                }
                            }
                        }
                    }
                    counterIdSolicitud[idEmpleadoArr[i].nombre];
                }
            }

            // res.status(200).send( counter );
            res.status(200).send( counterSubareaName );

        } catch (e) {
            console.log(e, 'errooooooooooooooooooooooooor-----------------------');
            manejoErrores('No se encontró el idEmpleado', e, 404);
        }

        function removeDuplicates(originalArray, prop) {
            // console.log(originalArray, prop, '-----------------------');
            var newArray = [];
            var lookupObject  = {};

            for(var i in originalArray) {
                lookupObject[originalArray[i][prop]] = originalArray[i];
            }

            for(i in lookupObject) {
                newArray.push(lookupObject[i]);
            }
            return newArray;
        }
    };

    static getCanalesPorEjecutivo = async (req: Request, res: Response) => {
        const idEmpleado = req.params.idEmpleado;
        const mensajesWhatsappData = await getManager().query(`SELECT * FROM empleadosMensajesWhatsappView WHERE idEmpleadoEnvio = ${idEmpleado} AND idTipoMensajeWhatsapp = 7;`);

        let data;
        const ttt = await mensajesWhatsappData.map( d => {
            return new Promise( async function (resolve, reject) {
                const dataMensajes = await getManager().query(`
                SELECT
                    canalWhatsapp.id,
                    canalWhatsapp.idSolicitud,
                    canalWhatsapp.origen,
                    canalWhatsapp.numerosRestantes,
                    mensajesWhatsapp.idTipoMensajeWhatsapp,
                    mensajesWhatsapp.fecha,
                    mensajesWhatsapp.mensaje,
                    mensajesWhatsapp.idTipoEnvio,
                    mensajesWhatsapp.idEmpleadoEnvio,
                    mensajesWhatsapp.idEmpleadoMensajes,
                    mensajesWhatsapp.aBandeja
                FROM
                    canalWhatsapp
                INNER JOIN mensajesWhatsapp ON canalWhatsapp.id = mensajesWhatsapp.idCanalWhatsapp
                WHERE 
                    mensajesWhatsapp.idEmpleadoMensajes = ${d.idEmpleado} AND
                    canalWhatsapp.idSolicitud = ${d.correlationId}
            `);
                // console.log(dataMensajes, '*****************************');
                switch (dataMensajes.idTipoEnvio) {
                    case 1:
                        resolve({...d, respondido: true});
                        break;
                    case 2:
                        resolve({...d, respondido: false});
                        break;
                    default:
                        break;
                }
                // console.log(data, '------------------------');
                // return data;
            });
        });


        console.log(ttt, 'ttttttttttttttttttttttttttttttttt');
        res.status(200).send( mensajesWhatsappData );
    };

    static getEjecutivosPosSubarea = async (req: Request, res: Response) => {
        const fechaInicio = req.query.fechaInicio;
        const fechaFin = req.query.fechaFin;
        const idSubarea = req.params.idSubarea;
        const ejecutivosOperacionWhatsappViewData = await getManager().query(`SELECT * FROM ejecutivosOperacionWhatsappView WHERE idSubarea = ${idSubarea} AND fecha BETWEEN '${fechaInicio}' AND '${fechaFin}';`);
        // console.log(fechaInicio, fechaFin, ejecutivosOperacionWhatsappViewData, '********************');
        const arrDataEjecutivos = [];
        ejecutivosOperacionWhatsappViewData.forEach(dataEjecutivos => {
            arrDataEjecutivos.push({
                nombre: `${dataEjecutivos.nombre} ${dataEjecutivos.paterno} ${dataEjecutivos.materno}`,
                idSubarea: dataEjecutivos.idSubarea,
                idTipoSubarea: dataEjecutivos.idTipoSubarea,
                idPuesto: dataEjecutivos.idPuesto,
                idEmpleado: dataEjecutivos.idEmpleadoEnvio,
            });
        });
        // console.log(ejecutivosOperacionWhatsappViewData);

        res.status(200).send( arrDataEjecutivos );
    };

    static getSubareasGralDos = async (req: Request, res: Response) => {

        const fechaInicio = req.query.fechaInicio;
        const fechaFin = req.query.fechaFin;

        // const mensajesWhatappRepository = getRepository(mensajesWhatsapp);
        // const mensajesWhatsappData = await mensajesWhatappRepository.find();
        // const empleadosMensajesWhatsappViewRepository = await getRepository(EmpleadosMensajesWhatsappView).manager.query(`SELECT * FROM empleadosMensajesWhatsappView WHERE fecha BETWEEN '${fechaInicio}' AND '${fechaFin}';`);

        try {
            let counterSubareaName = {};
            const subareaName = [];

            const subareaData = await getRepository(SubareasOperacionWhatsappView).find();
            // for (let h = 0; h < subareaData.length; h++) {
            // const ejecutivosOperacionWhatsappViewData = await getRepository(EjecutivosOperacionWhatsappView).manager.query(`SELECT * FROM ejecutivosOperacionWhatsappView WHERE idTipoSubarea = ${subareaData[h].idTipoSubarea} AND idSubarea = ${subareaData[h].idSubarea};`);
            // const ejecutivosOperacionWhatsappViewData = await getRepository(EjecutivosOperacionWhatsappView).find();
            // console.log(ejecutivosOperacionWhatsappViewData);
            // getManager().query('');
            const idEmpleadoArr = [];
            // ejecutivosOperacionWhatsappViewData.forEach(dataMsg => {
            //     idEmpleadoArr.push({nombre: dataMsg.nombre + ' ' + dataMsg.paterno + ' ' + dataMsg.materno, idCanalWhatsapp: +dataMsg.idCanalWhatsapp, idEmpleado: +dataMsg.idEmpleado, idSolicitud: +dataMsg.correlationId, fecha: dataMsg.fecha, subarea: dataMsg.subarea, idSubarea: dataMsg.idSubarea});
            // });

            let counterIdSolicitud = {};
            const arrAuxIdSolicitud = [];


            const arrSubareaName = [];
            subareaData.forEach(dataSupervisiores => {
                arrSubareaName.push({subarea: dataSupervisiores.subarea, idSubarea: dataSupervisiores.idSubarea});
            });

            const arrAuxSubareaName = [];

            console.log(arrSubareaName, 'arrSubareaNamearrSubareaName')



            for(let k = 0; k < arrSubareaName.length; k++) {
                if(!(arrSubareaName[k].subarea in counterSubareaName)) {
                    arrSubareaName.forEach(data => {
                        if (data.subarea === arrSubareaName[k].subarea) { // se compararía con la propiedad subarea de los mesajes
                            arrAuxSubareaName.push(data);
                            counterSubareaName[arrAuxSubareaName[k].subarea] = {};
                        }
                    });
                }
            }


            // for(let i = 0; i < idEmpleadoArr.length; i++) {
            //     if(!(idEmpleadoArr[i].nombre in counterIdSolicitud)) {
            //         idEmpleadoArr.forEach(data => {
            //             if (data.nombre === idEmpleadoArr[i].nombre) {
            //                 arrAuxIdSolicitud.push(data);
            //             }
            //         });
            //
            //         let arrrIdSolicitud = [];
            //         const retIdSolicitud = arrAuxIdSolicitud.filter(res => {
            //             return res.nombre === idEmpleadoArr[i].nombre;
            //         });
            //
            //         for (let i = 0; i < retIdSolicitud.length; i++) {
            //             const prueba: any = {};
            //             const pruebaIdSolicitud: any = {};
            //             prueba['idEmpleado'] = retIdSolicitud[i].idEmpleado;
            //             prueba['subarea'] = retIdSolicitud[i].subarea;
            //             prueba['idSubarea'] = retIdSolicitud[i].idSubarea;
            //             // prueba['fechaCreacion'] = retIdSolicitud[i].fecha;
            //             // prueba['idSolicitud'] = +retIdSolicitud[i].idSolicitud;
            //             // prueba['responder'] = responderData.length === 1 ? 0 : responderData[responderData.length - 1].idTipoEnvio === 2;
            //             const test = empleadosMensajesWhatsappViewRepository.filter(res => {
            //                 return +res.idEmpleadoEnvio === +retIdSolicitud[i].idEmpleado
            //             });
            //             const test2 = removeDuplicates(test, 'correlationId');
            //             if (test2.length !== 0) {
            //                 prueba['canalesActivos'] = [{responder: test2.length === 1 ? 0 : test2[test2.length - 1].idTipoEnvio === 2}, ...test2];
            //             }
            //             arrrIdSolicitud.push(prueba);
            //             arrrIdSolicitud = removeDuplicates(arrrIdSolicitud, retIdSolicitud[i].idSolicitud);
            //         }
            //
            //         counterIdSolicitud[idEmpleadoArr[i].nombre] = [arrrIdSolicitud[0]['subarea'], ...arrrIdSolicitud];
            //
            //         let claves = Object.keys(counterIdSolicitud);
            //         let clavesSubarea = Object.keys(counterSubareaName);
            //         for(let i=0; i< claves.length; i++){
            //             let clave = claves[i];
            //             for(let j=0; j< clavesSubarea.length; j++){
            //                 let claveSubarea = clavesSubarea[j];
            //                 if (counterIdSolicitud[clave][0] === clavesSubarea[j]) {
            //                     counterSubareaName[clavesSubarea[j]] = counterIdSolicitud;
            //                 }
            //             }
            //         }
            //     }
            //     counterIdSolicitud[idEmpleadoArr[i].nombre];
            // }
            // }

            // res.status(200).send( counter );
            // res.status(200).send( counterSubareaName );
            res.status(200).send( arrSubareaName );

        } catch (e) {
            console.log(e, 'errooooooooooooooooooooooooor-----------------------');
            manejoErrores('No se encontró el idEmpleado', e, 404);
        }

        function removeDuplicates(originalArray, prop) {
            // console.log(originalArray, prop, '-----------------------');
            var newArray = [];
            var lookupObject  = {};

            for(var i in originalArray) {
                lookupObject[originalArray[i][prop]] = originalArray[i];
            }

            for(i in lookupObject) {
                newArray.push(lookupObject[i]);
            }
            return newArray;
        }
    };

    static getSubareasGral = async (req: Request, res: Response) => {
        // const fechaInicio = req.query.fechaInicio;
        // const fechaFin = req.query.fechaFin;
        //
        // try {
        //     const subareaData = await getRepository(SubareasOperacionWhatsappView).find();
        //     const mensajesWhatsappData = await getRepository(mensajesWhatsapp).find();
        //     const campanasJson = {};
        //     for (let i = 0; i < subareaData.length; i++) {
        //         campanasJson[subareaData[i].subarea] = {};
        //         const arrEjecutivos = [];
        //         // const empleadosMensajesWhatsappViewData = await getRepository(EmpleadosMensajesWhatsappView).manager.query(`SELECT * FROM empleadosMensajesWhatsappView WHERE idTipoSubarea = ${subareaData[i].idTipoSubarea} AND idSubarea = ${subareaData[i].idSubarea} AND fecha BETWEEN '${fechaInicio}' AND '${fechaFin}';`);
        //         const ejecutivosOperacionWhatsappViewData = await getRepository(EjecutivosOperacionWhatsappView).manager.query(`SELECT * FROM ejecutivosOperacionWhatsappView WHERE idTipoSubarea = ${subareaData[i].idTipoSubarea} AND idSubarea = ${subareaData[i].idSubarea};`);
        //         let counter = {};
        //         for (let j = 0; j < ejecutivosOperacionWhatsappViewData.length; j++) {
        //             const {nombre, paterno, materno} = ejecutivosOperacionWhatsappViewData[j];
        //             const nombreEje = `${nombre} ${paterno} ${materno}`;
        //             if(!(nombreEje in counter)) {
        //                 counter[ejecutivosOperacionWhatsappViewData[j].nombre] = {};
        //             }
        //         }
        //
        //         console.log(counter, 'countercounter');
        //         campanasJson[subareaData[i].subarea] = counter;
        //     }
        //     res.status(200).send( campanasJson );
        // } catch (e) {
        //     console.log(e, 'Salió un error')
        // }
        // function removeDuplicates(originalArray, prop) {
        //     var newArray = [];
        //     var lookupObject  = {};
        //
        //     for(var i in originalArray) {
        //         lookupObject[originalArray[i][prop]] = originalArray[i];
        //     }
        //
        //     for(i in lookupObject) {
        //         newArray.push(lookupObject[i]);
        //     }
        //     return newArray;
        // }









        const fechaInicio = req.query.fechaInicio;
        const fechaFin = req.query.fechaFin;

        // const mensajesWhatappRepository = getRepository(mensajesWhatsapp);
        // const mensajesWhatsappData = await mensajesWhatappRepository.find();
        // const empleadosMensajesWhatsappViewRepository = await getRepository(EmpleadosMensajesWhatsappView).manager.query(`SELECT * FROM empleadosMensajesWhatsappView WHERE fecha BETWEEN '${fechaInicio}' AND '${fechaFin}';`);

        try {
            let counterSubareaName = {};

            const subareaData = await getRepository(SubareasOperacionWhatsappView).find();
            // for (let h = 0; h < subareaData.length; h++) {
                // const ejecutivosOperacionWhatsappViewData = await getRepository(EjecutivosOperacionWhatsappView).manager.query(`SELECT * FROM ejecutivosOperacionWhatsappView WHERE idTipoSubarea = ${subareaData[h].idTipoSubarea} AND idSubarea = ${subareaData[h].idSubarea};`);
                // const ejecutivosOperacionWhatsappViewData = await getRepository(EjecutivosOperacionWhatsappView).find();
                // console.log(ejecutivosOperacionWhatsappViewData);
                // getManager().query('');
                const idEmpleadoArr = [];
                // ejecutivosOperacionWhatsappViewData.forEach(dataMsg => {
                //     idEmpleadoArr.push({nombre: dataMsg.nombre + ' ' + dataMsg.paterno + ' ' + dataMsg.materno, idCanalWhatsapp: +dataMsg.idCanalWhatsapp, idEmpleado: +dataMsg.idEmpleado, idSolicitud: +dataMsg.correlationId, fecha: dataMsg.fecha, subarea: dataMsg.subarea, idSubarea: dataMsg.idSubarea});
                // });

                let counterIdSolicitud = {};
                const arrAuxIdSolicitud = [];


                const arrSubareaName = [];
                subareaData.forEach(dataSupervisiores => {
                    arrSubareaName.push({subarea: dataSupervisiores.subarea});
                });

                const arrAuxSubareaName = [];



                for(let k = 0; k < arrSubareaName.length; k++) {
                    if(!(arrSubareaName[k].subarea in counterSubareaName)) {
                        arrSubareaName.forEach(data => {
                            if (data.subarea === arrSubareaName[k].subarea) { // se compararía con la propiedad subarea de los mesajes
                                arrAuxSubareaName.push(data);
                                counterSubareaName[arrAuxSubareaName[k].subarea] = {};
                            }
                        });
                    }
                }


                // for(let i = 0; i < idEmpleadoArr.length; i++) {
                //     if(!(idEmpleadoArr[i].nombre in counterIdSolicitud)) {
                //         idEmpleadoArr.forEach(data => {
                //             if (data.nombre === idEmpleadoArr[i].nombre) {
                //                 arrAuxIdSolicitud.push(data);
                //             }
                //         });
                //
                //         let arrrIdSolicitud = [];
                //         const retIdSolicitud = arrAuxIdSolicitud.filter(res => {
                //             return res.nombre === idEmpleadoArr[i].nombre;
                //         });
                //
                //         for (let i = 0; i < retIdSolicitud.length; i++) {
                //             const prueba: any = {};
                //             const pruebaIdSolicitud: any = {};
                //             prueba['idEmpleado'] = retIdSolicitud[i].idEmpleado;
                //             prueba['subarea'] = retIdSolicitud[i].subarea;
                //             prueba['idSubarea'] = retIdSolicitud[i].idSubarea;
                //             // prueba['fechaCreacion'] = retIdSolicitud[i].fecha;
                //             // prueba['idSolicitud'] = +retIdSolicitud[i].idSolicitud;
                //             // prueba['responder'] = responderData.length === 1 ? 0 : responderData[responderData.length - 1].idTipoEnvio === 2;
                //             const test = empleadosMensajesWhatsappViewRepository.filter(res => {
                //                 return +res.idEmpleadoEnvio === +retIdSolicitud[i].idEmpleado
                //             });
                //             const test2 = removeDuplicates(test, 'correlationId');
                //             if (test2.length !== 0) {
                //                 prueba['canalesActivos'] = [{responder: test2.length === 1 ? 0 : test2[test2.length - 1].idTipoEnvio === 2}, ...test2];
                //             }
                //             arrrIdSolicitud.push(prueba);
                //             arrrIdSolicitud = removeDuplicates(arrrIdSolicitud, retIdSolicitud[i].idSolicitud);
                //         }
                //
                //         counterIdSolicitud[idEmpleadoArr[i].nombre] = [arrrIdSolicitud[0]['subarea'], ...arrrIdSolicitud];
                //
                //         let claves = Object.keys(counterIdSolicitud);
                //         let clavesSubarea = Object.keys(counterSubareaName);
                //         for(let i=0; i< claves.length; i++){
                //             let clave = claves[i];
                //             for(let j=0; j< clavesSubarea.length; j++){
                //                 let claveSubarea = clavesSubarea[j];
                //                 if (counterIdSolicitud[clave][0] === clavesSubarea[j]) {
                //                     counterSubareaName[clavesSubarea[j]] = counterIdSolicitud;
                //                 }
                //             }
                //         }
                //     }
                //     counterIdSolicitud[idEmpleadoArr[i].nombre];
                // }
            // }

            // res.status(200).send( counter );
            res.status(200).send( counterSubareaName );

        } catch (e) {
            console.log(e, 'errooooooooooooooooooooooooor-----------------------');
            manejoErrores('No se encontró el idEmpleado', e, 404);
        }

        function removeDuplicates(originalArray, prop) {
            // console.log(originalArray, prop, '-----------------------');
            var newArray = [];
            var lookupObject  = {};

            for(var i in originalArray) {
                lookupObject[originalArray[i][prop]] = originalArray[i];
            }

            for(i in lookupObject) {
                newArray.push(lookupObject[i]);
            }
            return newArray;
        }
    };

    static getSubareas = async (req: Request, res: Response) => {
        const fechaInicio = req.query.fechaInicio;
        const fechaFin = req.query.fechaFin;

        const subareasPlantillaActivaRepository = getRepository(SubareasOperacionWhatsappView);
        try {
            const subareaData = await subareasPlantillaActivaRepository.find();
            // const supervisoresTipoSubareaRepository = await getRepository(SupervisoresSubareaWhatsappView);
            // const mensajesWhatappRepository = getRepository(mensajesWhatsapp);
            // const mensajesWhatsappData = await mensajesWhatappRepository.find();
            let supervisores: any[];
            const campanas = {};
            // const auxSupervisor = {};
            let sinDuplicados: any;
            for (let i = 0; i < subareaData.length; i++) {
                // supervisores = await getRepository(SupervisoresSubareaWhatsappView).manager.query(`SELECT * FROM supervisoresSubareaWhatsappView WHERE idTipoSubarea = ${subareaData[i].idTipoSubarea} AND idSubarea = ${subareaData[i].idSubarea};`);
                campanas[subareaData[i].subarea] = {};
                const empleadosMensajesWhatsappViewRepository = await getRepository(EmpleadosMensajesWhatsappView).manager.query(`SELECT * FROM empleadosMensajesWhatsappView WHERE idTipoSubarea = ${subareaData[i].idTipoSubarea} AND idSubarea = ${subareaData[i].idSubarea} AND fecha BETWEEN '${fechaInicio}' AND '${fechaFin}';`);
                const empleadoMensajesWhatsappSinDuplicados = removeDuplicates(empleadosMensajesWhatsappViewRepository, 'idEmpleado');
                campanas[subareaData[i].subarea] = empleadoMensajesWhatsappSinDuplicados.map(data => {
                    const { nombre, paterno, materno, idEmpleado} = data;
                    const keyNombreEjecutivo = {};
                    const solicitudes = empleadosMensajesWhatsappViewRepository.filter(res => {
                        if (res.idEmpleado === idEmpleado) {
                            return res;
                        }
                    });
                    // console.log(solicitudes, 'solicitudessolicitudessolicitudes')
                    keyNombreEjecutivo[`${nombre} ${paterno} ${materno}`] = solicitudes;
                        /*empleadosMensajesWhatsappViewRepository.filter(EMW => {
                        const arr = [];
                        if (EMW.idEmpleadoEnvio === idEmpleado) {
                            arr.push(EMW);
                        }
                        return arr;
                    });*/
                    return keyNombreEjecutivo;
                });
                // return campanas;
                // });
                // campanas[subareaData[i].subarea] = sinDuplicados.map( data => {
                //     // console.log(data, 'datadatadatadatadata')
                //     const auxSupervisor = {};
                //     const {nombreSupervisor, paternoSupervisor, maternoSupervisor, idTipoSubarea, idSubarea} = data;
                //     // console.log(nombreSupervisor === 'KARLA JANETH' ? data : '-');
                //     auxSupervisor[`${nombreSupervisor} ${paternoSupervisor} ${maternoSupervisor}`] = {};
                //     /****************************************************************************************************/
                //     // console.log(empleadosMensajesWhatsappViewRepository, 'empleadosMensajesWhatsappViewRepositoryempleadosMensajesWhatsappViewRepository');
                //     // auxSupervisor[`${nombreSupervisor} ${paternoSupervisor} ${maternoSupervisor}`] = await getRepository(EmpleadosMensajesWhatsappView).manager.query(`SELECT * FROM empleadosMensajesWhatsappView WHERE idTipoSubarea = ${idTipoSubarea};`);;
                //
                //     getRepository(empleadoView).manager.query(`SELECT * FROM empleadoView WHERE idTipoSubarea = ${idTipoSubarea} AND idSubarea = ${idSubarea} AND idPuesto = 8;`).then(
                //         ejecutivosData => {
                //             // console.log(ejecutivosData.length);
                //             const idEmpleadoArr = [];
                //             ejecutivosData.forEach(dataEjecutivoIndividual => {
                //                 idEmpleadoArr.push({nombre: dataEjecutivoIndividual.nombre + ' ' + dataEjecutivoIndividual.apellidoPaterno + ' ' + dataEjecutivoIndividual.apellidoMaterno, idEmpleado: +dataEjecutivoIndividual.id, subarea: dataEjecutivoIndividual.subarea, idSubarea: dataEjecutivoIndividual.idSubarea, idTipoSubarea: dataEjecutivoIndividual.idTipoSubarea});
                //             });
                //             auxSupervisor[`${nombreSupervisor} ${paternoSupervisor} ${maternoSupervisor}`] = idEmpleadoArr.map(dEje => {
                //                 const { nombre, idEmpleado } = dEje;
                //                 const auxEjecutivoNombre = {};
                //                 auxEjecutivoNombre[nombre] = {};
                //                 getRepository(EmpleadosMensajesWhatsappView).manager.query(`SELECT * FROM empleadosMensajesWhatsappView WHERE idTipoSubarea = ${idTipoSubarea} AND idSubarea = ${idSubarea} AND idEmpleado = ${idEmpleado} AND fecha BETWEEN '${fechaInicio}' AND '${fechaFin}';`).then(
                //                     dataMensajesPorEjecutivo => {
                //                         if (dataMensajesPorEjecutivo.length > 0) {
                //                             const auxMensajesPorEjecutivo = {};
                //                             // auxEjecutivoNombre[nombre] = removeDuplicates(dataMensajesPorEjecutivo, 'correlationId');
                //                             const dataMensajes = removeDuplicates(dataMensajesPorEjecutivo, 'correlationId');
                //                             const { correlationId } = dataMensajes[0];
                //                             auxEjecutivoNombre[nombre] = correlationId;
                //                         }
                //                     });
                //
                //                 return auxEjecutivoNombre;
                //             });
                //         });
                //     // getRepository(EmpleadosMensajesWhatsappView).manager.query(`SELECT * FROM empleadosMensajesWhatsappView WHERE idTipoSubarea = ${idTipoSubarea} AND idSubarea = ${idSubarea};`)
                //     //     .then(data => {
                //     //         const idEmpleadoArr = [];
                //     //         data.forEach(dataMsg => {
                //     //             idEmpleadoArr.push({nombre: dataMsg.nombre + ' ' + dataMsg.paterno + ' ' + dataMsg.materno, idCanalWhatsapp: +dataMsg.idCanalWhatsapp, idEmpleado: +dataMsg.idEmpleado, idSolicitud: +dataMsg.correlationId, fecha: dataMsg.fecha, subarea: dataMsg.subarea, idSubarea: dataMsg.idSubarea});
                //     //         });
                //     //
                //     //         // console.log(idEmpleadoArr, 'idEmpleadoArridEmpleadoArridEmpleadoArridEmpleadoArr');
                //     //
                //     //         /*let counterIdSolicitud = {};
                //     //         const arrAuxIdSolicitud = [];
                //     //         for(let i = 0; i < idEmpleadoArr.length; i++) {
                //     //                 if(!(idEmpleadoArr[i].nombre in counterIdSolicitud)) {
                //     //                     idEmpleadoArr.forEach(data2 => {
                //     //                         if (data2.nombre === idEmpleadoArr[i].nombre) {
                //     //                             arrAuxIdSolicitud.push(data2);
                //     //                         }
                //     //                     });
                //     //
                //     //                     let arrrIdSolicitud = [];
                //     //                     const retIdSolicitud = arrAuxIdSolicitud.filter(res => {
                //     //                         return res.nombre === idEmpleadoArr[i].nombre;
                //     //                     });
                //     //
                //     //                     for (let i = 0; i < retIdSolicitud.length; i++) {
                //     //                         const json: any = {};
                //     //                         json[+retIdSolicitud[i].idSolicitud] = mensajesWhatsappData.filter(res => {
                //     //                             return res.correlationId === +retIdSolicitud[i].idSolicitud;
                //     //                         });
                //     //                         const responderData = mensajesWhatsappData.filter(res => {
                //     //                             return res.correlationId === +retIdSolicitud[i].idSolicitud;
                //     //                         });
                //     //                         json['subarea'] = retIdSolicitud[i].subarea;
                //     //                         json['idSubarea'] = retIdSolicitud[i].idSubarea;
                //     //                         json['fechaCreacion'] = retIdSolicitud[i].fecha;
                //     //                         json['idSolicitud'] = +retIdSolicitud[i].idSolicitud;
                //     //                         json['responder'] = responderData.length === 1 ? 0 : responderData[responderData.length - 1].idTipoEnvio === 2;
                //     //                         arrrIdSolicitud.push(json);
                //     //                         arrrIdSolicitud = removeDuplicates(arrrIdSolicitud, retIdSolicitud[i].idSolicitud);
                //     //                     }
                //     //
                //     //                     counterIdSolicitud[idEmpleadoArr[i].nombre] = [arrrIdSolicitud[0]['subarea'], ...arrrIdSolicitud];
                //     //         }
                //     //         auxSupervisor[`${nombreSupervisor} ${paternoSupervisor} ${maternoSupervisor}`] = arrAuxIdSolicitud;
                //     //     }*/
                //     //     // return data;
                //     // }).catch(e => {
                //     //     console.log(e, 'error en dataMensajes')
                //     // });
                //
                //     /****************************************************************************************************/
                //     return auxSupervisor;
                // });
            }
            res.status(200).send( campanas );
        } catch (e) {
            console.log(e, 'Error al cargar las subareas');
        }

        function removeDuplicates(originalArray, prop) {
            var newArray = [];
            var lookupObject  = {};

            for(var i in originalArray) {
                lookupObject[originalArray[i][prop]] = originalArray[i];
            }

            for(i in lookupObject) {
                newArray.push(lookupObject[i]);
            }
            return newArray;
        }
    }

    // static getMensajesDeEjecutivosAdministrador = async (req: Request, res: Response) => {
    //     const idEmpleado = req.query.idEmpleado;
    //     const idPuesto = req.query.idPuesto;
    //     const idSubarea = req.query.idSubarea;
    //     const fechaInicio = req.query.fechaInicio;
    //     const fechaFin = req.query.fechaFin;
    //
    //     const subareaRepository = getRepository(Subarea);
    //     const tipoSubareaRepository = getRepository(TipoSubarea);
    //     const supervisoresTipoSubareaRepository = await getRepository(SupervisoresSubareaWhatsappView);
    //     const mensajesWhatappRepository = getRepository(mensajesWhatsapp);
    //     const mensajesWhatsappData = await mensajesWhatappRepository.find();
    //     const tipoSubareaData = await tipoSubareaRepository.find();
    //
    //     try {
    //         const subareaData = await subareaRepository.find();
    //
    //         const empleadosMensajesWhatsappViewRepository = getRepository(EmpleadosMensajesWhatsappView).manager.query(`SELECT * FROM empleadosMensajesWhatsappView WHERE idTipoSubarea = ${idTipoSubareaSupervisor} AND fecha BETWEEN '${fechaInicio}' AND '${fechaFin}';`);
    //
    //         const dataMensajes = await empleadosMensajesWhatsappViewRepository.then(data => {
    //             return data;
    //         }).catch(e => {
    //             console.log(e, 'error en dataMensajes')
    //         });
    //         const idEmpleadoArr = [];
    //         dataMensajes.forEach(dataMsg => {
    //             idEmpleadoArr.push({nombre: dataMsg.nombre + ' ' + dataMsg.paterno + ' ' + dataMsg.materno, idCanalWhatsapp: +dataMsg.idCanalWhatsapp, idEmpleado: +dataMsg.idEmpleado, idSolicitud: +dataMsg.correlationId, fecha: dataMsg.fecha, subarea: dataMsg.subarea, idSubarea: dataMsg.idSubarea});
    //         });
    //
    //         // cuenta cuantos mensajes tienen por responder cada canal
    //         // { '161552': 1, '161553': 1, '161555': 2 }
    //         // const idEmpleadoArrArrSorted = idEmpleadoArr.sort();
    //
    //         let counterIdSolicitud = {};
    //         const arrAuxIdSolicitud = [];
    //
    //         let counterSubareaName = {};
    //
    //             const arrSubareaName = [];
    //             supervisoresTipoSubareaRepository.forEach(dataSupervisiores => {
    //                 arrSubareaName.push({subarea: dataSupervisiores.subarea});
    //             });
    //             const arrAuxSubareaName = [];
    //
    //
    //         for(let k = 0; k < arrSubareaName.length; k++) {
    //         if(!(arrSubareaName[k].subarea in counterSubareaName)) {
    //             arrSubareaName.forEach(data => {
    //                 if (data.subarea === arrSubareaName[k].subarea) { // se compararía con la propiedad subarea de los mesajes
    //                     arrAuxSubareaName.push(data);
    //                     counterSubareaName[arrAuxSubareaName[k].subarea] = {};
    //                 }
    //             });
    //         }
    //         }
    //
    //
    //         for(let i = 0; i < idEmpleadoArr.length; i++) {
    //             if(!(idEmpleadoArr[i].nombre in counterIdSolicitud)) {
    //                 idEmpleadoArr.forEach(data => {
    //                     if (data.nombre === idEmpleadoArr[i].nombre) {
    //                         arrAuxIdSolicitud.push(data);
    //                     }
    //                 });
    //
    //                 let arrrIdSolicitud = [];
    //                 const retIdSolicitud = arrAuxIdSolicitud.filter(res => {
    //                     return res.nombre === idEmpleadoArr[i].nombre;
    //                 });
    //
    //                 for (let i = 0; i < retIdSolicitud.length; i++) {
    //                     const prueba: any = {};
    //                     prueba[+retIdSolicitud[i].idSolicitud] = mensajesWhatsappData.filter(res => {
    //                         return res.correlationId === +retIdSolicitud[i].idSolicitud;
    //                     });
    //                     const responderData = mensajesWhatsappData.filter(res => {
    //                         return res.correlationId === +retIdSolicitud[i].idSolicitud;
    //                     });
    //                     prueba['subarea'] = retIdSolicitud[i].subarea;
    //                     prueba['idSubarea'] = retIdSolicitud[i].idSubarea;
    //                     prueba['fechaCreacion'] = retIdSolicitud[i].fecha;
    //                     prueba['idSolicitud'] = +retIdSolicitud[i].idSolicitud;
    //                     prueba['responder'] = responderData.length === 1 ? 0 : responderData[responderData.length - 1].idTipoEnvio === 2;
    //                     arrrIdSolicitud.push(prueba);
    //                     arrrIdSolicitud = removeDuplicates(arrrIdSolicitud, retIdSolicitud[i].idSolicitud);
    //                 }
    //
    //                 counterIdSolicitud[idEmpleadoArr[i].nombre] = [arrrIdSolicitud[0]['subarea'], ...arrrIdSolicitud];
    //
    //                 let claves = Object.keys(counterIdSolicitud);
    //                 let clavesSubarea = Object.keys(counterSubareaName);
    //                 for(let i=0; i< claves.length; i++){
    //                     let clave = claves[i];
    //                     for(let j=0; j< clavesSubarea.length; j++){
    //                         let claveSubarea = clavesSubarea[j];
    //                         if (counterIdSolicitud[clave][0] === clavesSubarea[j]) {
    //                             counterSubareaName[clavesSubarea[j]] = counterIdSolicitud;
    //                         }
    //                     }
    //                 }
    //             }
    //             counterIdSolicitud[idEmpleadoArr[i].nombre];
    //         }
    //         res.status(200).send( counterSubareaName );
    //
    //     } catch (e) {
    //         console.log(e, 'errooooooooooooooooooooooooor-----------------------');
    //         manejoErrores('No se encontró el idEmpleado', e, 404);
    //     }
    //
    //     function removeDuplicates(originalArray, prop) {
    //         var newArray = [];
    //         var lookupObject  = {};
    //
    //         for(var i in originalArray) {
    //             lookupObject[originalArray[i][prop]] = originalArray[i];
    //         }
    //
    //         for(i in lookupObject) {
    //             newArray.push(lookupObject[i]);
    //         }
    //         return newArray;
    //     }
    // }
}
// Functions
function manejoErrores(mensaje, res, codigo, cuerpo?) {
    res.send({
        cuerpo: String(cuerpo),
        mensaje: mensaje,
        codigo: codigo
    })
}

async function counter(idEmpleado?) {
    const idEmpleadoEnvio = idEmpleado;
    const mensajesWhatsappRepository = getRepository(mensajesWhatsapp);
    const mensajes = await mensajesWhatsappRepository.find({
        where: {aBandeja: 1, idTipoEnvio: 2, idEmpleadoMensajes: idEmpleadoEnvio}
    });
    const mensajesFiltroHora = mensajes.filter(data => {
        const date = new Date(data.fecha);
        const dateConHoras = new Date(data.fecha).setHours(24);
        const dateActual = new Date();
        const dateMsecActual = dateActual.getTime();

        const fechasData = conversion(date, dateConHoras, dateMsecActual);
        return fechasData.milisecActual < fechasData.milisecFechaRegistroMasHoras;
    });
    const noRespondidosArr = [];
    mensajesFiltroHora.forEach(dataMsg => {
        noRespondidosArr.push(dataMsg.correlationId);
    });
    /* cuenta cuantos mensajes tienen por responder cada canal
     { '161552': 1, '161553': 1, '161555': 2 } */
    const noRespondidosArrSorted = noRespondidosArr.sort();
    let counter = {};
    for (let i = 0; i < noRespondidosArrSorted.length; i++) {
        if (!(noRespondidosArrSorted[i] in counter)) counter[noRespondidosArrSorted[i]] = 0;
        counter[noRespondidosArrSorted[i]]++;
    }

    return counter;
}

function conversion(fecha, fechaMasHora, dateMsec) {
    const msecPerMinute = 1000 * 60;
    const msecPerHour = msecPerMinute * 60;
    const msecPerDay = msecPerHour * 24;

    const milisecFechaRegistro = fecha / msecPerDay;
    const milisecFechaRegistroMasHoras = fechaMasHora / msecPerDay;
    const milisecActual = dateMsec / msecPerDay;

    return {
        milisecFechaRegistro: milisecFechaRegistro,
        milisecFechaRegistroMasHoras: milisecFechaRegistroMasHoras,
        milisecActual: milisecActual
    }
}

function sendSocket(req, res) {
    const tipoSocket = req.socket;
    let tipoPath;
    const path = urlSockets.PATH;
    switch (tipoSocket) {
        case 'mensajes-and-canal':
            tipoPath = '/socket-mensajes-and-canal';
        break;

        case 'chat':
            tipoPath = '/socket-canal-whats-chat';
        break;

        case 'solicitudes':
            tipoPath = '/socket-solicitudes';
        break;

        case 'header':
            tipoPath = '/socket-header';
        break;

        case 'headerSolicitudes':
            tipoPath = '/socket-headerSolicitudes';
        break;
    }

    /*
    const data = JSON.stringify(req.body.message);
    // const data = JSON.stringify(req.body);
    const options: any = {
        method: 'POST',
        path: tipoPath,
        body: JSON.stringify(req.body),
        host: path,
    };
    const reeq = https.request(options, rees => {
        let payload = '';

        // se reciben los pedazos de info y se añade a 'str'
        rees.on('data', d => {
            payload += d;
        });
        // toda la respuesta ha sigo recibida, se manda en formato json
        rees.on('end', () => {
            res.json(JSON.parse(payload));
        });
    });
    reeq.on('error', error => {
        // manejador de errores
        const erroor = {...error, mensaje: 'algo salió mal'};
        res.send(erroor);
    });
    reeq.write(data);
    reeq.end();
    */


    /*const data = JSON.stringify({...req});
    const options = {
        hostname: path,
        // port: 8080,
        path: tipoPath,
        method: 'POST',
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method",
            'Content-Type': 'application/json',
        }
    };

    const request = http.request(options, res => {
        console.log(`statusCode: ${res.statusCode}`);

        res.on('data', d => {
            // console.log(`BODY: ${d}`);
        });
        res.on('end', () => {
            // console.log('No more data in response.');
        });
    }).on('error', error => {
        // console.error(error)
    });

    request.write(data);
    request.end();*/

    request({
            url: path + tipoPath,
            method: 'POST',
            json: true,
            body: req,
        },
        function (error, response, body) {
            // if (error) {
            //     console.log(error, 'errorerrorerrorerrorerror')
            //     // req.setTimeout(1800000);
            //     res.status(500).send();
            // }
            if (response) {
                // console.log(response.body, 'acabó...');
                console.log(response.body, 'response...');
            }
            if (body) {
                // console.log(body, 'body******************************');
            }
        });

    // console.log(req, 'reqreqreqreqreq')
    /*const url = path + tipoPath;
    // const cuerpo = JSON.stringify(req.body);
    fetch(url, {method: 'POST', body: req.body})
        .then(res => res.json())
        .then( json => {
            console.log(json, 'json')
            res.json(json)
            req.end();
        }).catch( e => {
        console.log(e, 'eeeeeeeeee')
    });*/
}
async function buscarCanal(req, res) {
    const numero = req.params.numero;

    const canalWhatsappRepository = getRepository(canalWhatsapp);
    try {
        const canalEncontrado = await canalWhatsappRepository.find({
            where: {destino: numero}
        });


        if (canalEncontrado.length !== 0) {
            return ({
                found: 1,
                data: canalEncontrado,
            });
        } else {
            return ({found: 0});
        }
    } catch (e) {
        return {
            message: `El número ${numero} no contiene ninguna información dentro de la tabla canalWhatsapp`,
            error: e,
        };
    }
}

async function getCatalogoNumerosWavy(idEmpresaMarca) {
    const catalogoNumerosWavyRepository = getRepository(CatalogoNumerosWavy);

    try {
        const catalogoNumerosWavyData = await catalogoNumerosWavyRepository.find({
            // where: {idMarca: 1}                 // LA MARCA ES AHORRA SEGUROS
            where: {idMarca: idEmpresaMarca}
        }).catch(e => {
            manejoErrores('No se encontró datos en el catálogo', e, 404);
        });
        return catalogoNumerosWavyData;
    } catch (e) {
        return {
            message: `No se encuentra información sobre los catálogos/credenciales de wavy`,
            error: e,
        };
    }
}

function numeroDataRandom(arr: any, numeroDestino?: any, numCliente?: any) {
    if (numeroDestino) {
        let index;
        let size = 0;
        for (let i = 0; i < arr.length; i++) {
            size = i;
        }
        index = Math.floor(Math.random() * (size));
        const daraReturn = arr.splice(index, 1);
        const arrNumerosWavyDb = {
            numeroGenerador: daraReturn[0].numeroTelefono,
            numerosRestantes: arr,
            namespace: daraReturn[0].namespace,
            userName: daraReturn[0].userName,
            authToken: daraReturn[0].authToken,
            id: daraReturn[0].id,
        };
        return this.numerosRestantes === '[]' ? null : arrNumerosWavyDb;
    } else {
        let index;
        let size = 0;
        for (let i = 0; i < arr.length; i++) {
            size = i;
        }
        index = Math.floor(Math.random() * (size));

        const daraReturn = arr.splice(index, 1);
        const arrNumerosWavyDb = {
            numeroGenerador: daraReturn[0].numeroTelefono,
            numerosRestantes: arr,
            namespace: daraReturn[0].namespace,
            userName: daraReturn[0].userName,
            authToken: daraReturn[0].authToken,
            id: daraReturn[0].id,
        };
        return arrNumerosWavyDb;
    }
}

async function getOrigenDestino(numDestino, numOrigen) {
    const numeroDestino = numDestino;
    const numeroOrigen = numOrigen;

    const canalWhatsappRepository = getRepository(canalWhatsapp);
    try {
        const canalEncontrado = await canalWhatsappRepository.findOne({
            where: {
                destino: numeroDestino,
                origen: numeroOrigen
            }
        });

        if (canalEncontrado) {
            return {
                // data: canalEncontrado
                found: 1,
                solicitud: canalEncontrado.idSolicitud,
                destino: canalEncontrado.destino,
                idCanal: canalEncontrado.id,
                numeroOrigen: canalEncontrado.origen,
                numerosRestantes: canalEncontrado.numerosRestantes
            };
        } else {
            return {found: 0};
        }
    } catch (e) {
        return {
            message: `No se encontró información con el número destino: ${numeroDestino}
                     y número origen ${numeroOrigen}`
        };
    }
}

async function getPlantillaByEmpleadoIdTipoSubarea(id, res) {
    const idEmpleado = id;

    // const tipoPlantillaRepository = getRepository(PlantillaComercialWhatsapp);
    const tipoSubareasPlantillaWhatsappRepository = getRepository(TipoSubareaPlantillaWhatsappView);
    const empleadoRepository = getRepository(empleadoView);

    try {
        const empleado: any = await empleadoRepository.findOneOrFail({
            where: {id: Number(idEmpleado)}
        }).catch(e => {
            manejoErrores('No se encontró el idEmpleado', e, 404);
        });
        const tipoSubareasPlantillaWhats = await tipoSubareasPlantillaWhatsappRepository.find({
            where: {
                idTipoSubarea: empleado.idTipoSubarea,
                activo: 1,
                idTipoPlantilla: 1,
            }
        }).catch(e => {
            manejoErrores('No se encontró tipoPlantilla', e, 404);
        });
        if (!tipoSubareasPlantillaWhats[0]) {
            res.status(404).json({
                error: 'El tipo subarea del empleado no está registrado para poder mandar plantillas',
                mensaje: 'Agregar tipo subarea a la tabla tipoSubareaPlantillaWhatsapp para poder enviar plantilla',
                extra: `tipo subarea intentando enviar plantilla: ${empleado.idTipoSubarea}`,
            });
        }
        return tipoSubareasPlantillaWhats;
    } catch (e) {
        return res.status(500).json({
                error: e
            });
    }
}

async function getNombreEmpleadoNombreProspecto(idEmpleado, idSolicitud) {
    try {
        const solicitudesViewRepository = getRepository(SolicitudesViewSinJson);
        // console.log(solicitudesViewRepository.findOneOrFail(idSolicitud), 'eeeeeeeeeeeeeeeeeeeeeeee')
        return solicitudesViewRepository.findOneOrFail(idSolicitud);
    } catch (e) {
        return e
    }
}

export default WhatsappController;
