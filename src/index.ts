// 'use strict';
import { Server, Socket } from 'socket.io';
import { createServer } from "http";
/** Imports **/
import "reflect-metadata";
import {createConnection, getConnectionOptions} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as helmet from "helmet";
import * as cors from "cors";
import routes from "./routes";
import {NamingStrategyCRM} from "./config/namingStrategyCRM";
import * as http from 'http';
import * as socketsWhatsapp from './sockets/Whatsapp/sockets-whatsapp'
const socketio = require('socket.io');
/** Config **/
// Create a new express application instance
const app = express();
// Se necessita usar el http para socket io
const httpServer = http.createServer(app);
// Inicializando socket io
/*export let tipoRoom;
export const io = socketio(httpServer, {
    cors: {
        transports : ['polling', 'websocket'],
        origins: ["https://trigarante2020.com/", "https://wavy.mark-43.net/", "http://localhost:4200/*"],
        methods: ['GET', 'POST'],
        extraHeaders: {
            // "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
        },
        secure: false,
        rejectUnauthorized: false
    }
});*/
const fileUpload = require('express-fileupload');

getConnectionOptions().then(connectionOptions => {
    return createConnection(Object.assign(connectionOptions, {
        namingStrategy: new NamingStrategyCRM()
    }))
});


/** Connects to the Database -> then starts the express **/
createConnection()
    .then(async connection => {
        /** Settings **/
        // Para que el puerto corra en el 8080 o en el que el servidor le de por default
        const puerto = process.env.PORT || 8080;
        app.set('port', puerto);
        // Call midlewares
        app.use(cors({credentials: false, origin: true}));
        app.use(helmet());
        app.use(bodyParser.json());
        // app.use(fileUpload()); // se mantienen los archivos guardados
        //Set all routes from routes folder
        app.use("/", routes);
        let idSolicitud = '';
        let idEmpleado = '';

        // Al iniciar el socket
        // io.on('connection', (socket) => {
        //     // identificador único para generar las salas
        //     // const socketID = socket.id;
        //     // viene desde el servicio de sockets en el front
        //     idSolicitud = socket.handshake.query.idSolicitud;
        //     idEmpleado = socket.handshake.query.idEmpleado;
        //
        //     // se genera una sala
        //     // tipoRoom = 'header';
        //     socket.on(`chatBtnHeader`, room => {
        //         tipoRoom = room;
        //     });
        //     socket.on(`chatRoom`, room => {
        //         if (room === 1) {
        //             tipoRoom = 'chat';
        //             socket.join(`room_chat_${idSolicitud}`);
        //         } else {
        //             tipoRoom = 'header';
        //             socket.leave(`room_chat_${idSolicitud}`);
        //         }
        //     });
        //     socket.on(`solicitudesRoom`, room => {
        //         if (room === 2) {
        //             tipoRoom = 'solicitudes';
        //             socket.join(`room_solicitudes_${idEmpleado}`);
        //         } else {
        //             tipoRoom = 'header';
        //             socket.leave(`room_solicitudes_${idEmpleado}`);
        //         }
        //     });
        //     socket.on(`headerRoom`, room => {
        //         if (room === `header-icon-${idEmpleado}`) {
        //             tipoRoom = 'header';
        //             socket.join(`room_${idEmpleado}`);
        //         } else {
        //             // socket.leave(`room_${idEmpleado}`);
        //         }
        //     });
        //     socket.on(`tablaMensajesPendientesRoom`, room => {
        //         if (room === `mostrarData-${idEmpleado}`) {
        //             tipoRoom = 'pendientes';
        //         } else {
        //             tipoRoom = 'header';
        //         }
        //     });
        //     // Aqui se maneja el socket de Whatsapp
        //     // socketsWhatsapp.mensaje(socket, io, idSolicitud);
        //
        //     // Si se desconecta del socket
        //     socketsWhatsapp.desconectado(socket);
        //     });

        httpServer.listen(app.get('port'), () => {
            console.log("Corre por puerto: ", app.get('port'));
        });
    })
    .catch(error => console.log(error));



/*

import Server from './config/server';
import router from './routes/index';
import * as bodyParser from "body-parser";
import * as cors from 'cors';



const server = Server.instance;

// BodyParser
server.app.use( bodyParser.urlencoded({ extended: true }) );
server.app.use(bodyParser.json());
// server.app.use( bodyParser.json() );

// CORS
server.app.use( cors({ origin: true, credentials: true  }) );


// Rutas de servicios
server.app.use('/', router );

*/


// server.start( () => {
//     console.log(`Servidor corriendo en el puerto ${ server.port }`);
// });


