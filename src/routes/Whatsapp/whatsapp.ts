import { Router } from 'express';
import WhatsappController from '../../controllers/whatsapp/WhatsappController';
const routes: Router = Router();

routes.post('/save/mo-response', WhatsappController.newMessage); // ya
routes.post('/send', WhatsappController.whatsappSend); // ya
routes.post('/send-plantilla', WhatsappController.whatsappSendPlantillas); // ya
routes.get('/send/sms', WhatsappController.sendSMS);
routes.get('/plantillas-whatsapp/tipo-subarea/:idEmpleado', WhatsappController.getPlantillaByIdEmpleadoTipoSubarea); // ya
routes.get('/mensajes/canal', WhatsappController.getMensajesByIdCanal);
routes.post('/solicitudes/numero-prospecto/:idSolicitud/:idEmpleado', WhatsappController.postCanalWhats);
routes.get('/chatTwice/:numero/:idCotizacionAli', WhatsappController.chatTwice);
routes.get('/canal-origen-destino/:numeroDestino/:numeroOrigen', WhatsappController.chatTwiceOrigenDestino);
routes.post('/solicitudes/send-plantilla', WhatsappController.postCanalWhatsPlantilla);
routes.post('/prueba', WhatsappController.pruebaRest);
routes.get('/catalogo-numeros-wavy', WhatsappController.getCatalogoNumeroWavy);
routes.get('/mensajes-canal-general/:idEmpleado', WhatsappController.getMensajesAndCanal);// <----
routes.put('/mensajes-respondido/:idEmpleado', WhatsappController.mensajeRespondidoSolicitudes);
routes.get('/mensajes-respondidos/:idEmpleado', WhatsappController.getMensajesRespondidos);
routes.put('/activar-canal-por-segunda-plantilla', WhatsappController.activarCanalPorSegundaPlantilla);
routes.post('/mensaje-por-segunda-plantilla', WhatsappController.postMensajePorReactivacionDeCanal);
routes.get('/mensajes-de-ejecutivos-por-supervisor', WhatsappController.getMensajesDeEjecutivosPorSupervisor);
routes.get('/mensajes-de-ejecutivos-por-supervisor-fecha', WhatsappController.getMensajesDeEjecutivosPorSupervisor);
routes.get('/subareas-activas', WhatsappController.getSubareas);
routes.get('/monitoreo-gral', WhatsappController.getSubareasGralDos);
routes.get('/ejecutivos-por-subarea/:idSubarea', WhatsappController.getEjecutivosPosSubarea);
routes.get('/canales-por-ejecutivos/:idEmpleado', WhatsappController.getCanalesPorEjecutivo);

export default routes;
