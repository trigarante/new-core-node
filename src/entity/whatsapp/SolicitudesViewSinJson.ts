import {Column, Entity, PrimaryColumn, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class SolicitudesViewSinJson {
    @PrimaryColumn()
    id: number;

    @Column()
    idCotizacionAli: number;

    @Column()
    idEmpleado: number;

    @Column()
    idEstadoSolicitud: number;

    @Column()
    fechaSolicitud: Date;

    @Column()
    estado: string;

    @Column()
    idSubArea: number;

    @Column()
    subArea: string;

    @Column()
    idTipoContacto: number;

    @Column()
    nombre: string;

    @Column()
    apellidoPaterno: string;

    @Column()
    apellidoMaterno: string;

    @Column()
    idProducto: number;

    @Column()
    idProspecto: number;

    @Column()
    nombreProspecto: string;

    @Column()
    numeroProspecto: string;

    @Column()
    etiquetaSolicitud: string;
}
