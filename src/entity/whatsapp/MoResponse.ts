import {Column, Entity, EntitySchema, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class moResponse {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    total: number;

    @Column()
    idWavy: string;

    @Column()
    idSolicitud: number;

    @Column({type: "json"})
    data: any;
}
