import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class SubareasOperacionWhatsappView {
    @PrimaryGeneratedColumn()
    idSubarea: number;

    @Column()
    subarea: string;

    @Column()
    idTipoSubarea: number;

    @Column()
    tipoSubarea: string;
}
