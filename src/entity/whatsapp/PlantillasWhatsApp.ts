import {Column, Entity, EntitySchema, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class plantillasWhatsApp {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre_plantilla: string;

    @Column()
    idTipoSubarea: number;

    @Column()
    activo: number;

    @Column()
    idTipoPlantilla: number;
}
