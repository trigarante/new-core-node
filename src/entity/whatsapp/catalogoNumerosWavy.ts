import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class CatalogoNumerosWavy {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    numeroTelefono: string;

    @Column()
    marca: string;

    @Column()
    tipo: string;

    @Column()
    namespace: string;

    @Column()
    userName: string;

    @Column()
    authToken: string;

    @Column()
    idMarca: number;

}
