import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class TipoMensajeWhatsapp {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    tipoMensaje: string;
}
