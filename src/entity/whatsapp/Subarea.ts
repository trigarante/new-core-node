import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class Subarea {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    idArea: number;

    @Column()
    subarea: string;

    @Column()
    descripcion: string;

    @Column()
    activo: number;

    @Column()
    codigo: number;

    @Column()
    idTipoSubarea: number;
}
