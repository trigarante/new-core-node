import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class TipoEnvio {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    tipo: string;
}
