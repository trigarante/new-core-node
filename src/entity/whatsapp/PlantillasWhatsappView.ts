import {Column, Entity, EntitySchema, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class plantillasWhatsappView {
    @PrimaryGeneratedColumn()
    idEmpleado: number;

    @Column()
    nombrePlantilla: string;

    @Column()
    activoPlantillas: number;

    @Column()
    idTipoPlantilla: number;

    @Column()
    descripcion: string;

    @Column()
    activoTipoPlantilla: number;

    @Column()
    idTipoSubarea: number;

    @Column()
    nombre: string;

    @Column()
    apellidoPaterno: string;

    @Column()
    apellidoMaterno: string;
}
