import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";


@Entity()
export class Socios {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    prioridad: number;

    @Column()
    nombreComercial: string;

    @Column()
    rfc: string;

    @Column()
    razonSocial: string;

    @Column()
    alias: string;

    @Column()
    idEstadoSocio: number;

    @Column()
    activo: number;

    @Column()
    idPais: number;

    @Column()
    expresionRegular: string;

    @Column()
    ejemploExpresionRegular: string;

    @Column()
    imagenPlantilla: string;
}
